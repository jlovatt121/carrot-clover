﻿using System;

using Android.App;
using Android.Content;

namespace CarrotClover
{
	public static class PlayerPrefs
	{
		private static ISharedPreferences playerPrefs = Application.Context.GetSharedPreferences ("CarrotCloverPlayerPrefs", FileCreationMode.Private);

		public static void SetString (string value, string key)
		{
			playerPrefs.Edit ().PutString (key, value).Commit();
		}

		public static string GetString (string key)
		{
			return playerPrefs.GetString (key,"");
		}

		public static string GetString (string key, string defValue)
		{
			return playerPrefs.GetString (key,defValue);
		}


		public static void SetInt (int value, string key)
		{
			playerPrefs.Edit ().PutInt (key, value).Commit();
		}

		public static int GetInt (string key)
		{
			return playerPrefs.GetInt (key,0);
		}

		public static int GetInt (string key, int defValue)
		{
			return playerPrefs.GetInt (key, defValue);
		}



		public static void SetBool (bool value, string key)
		{
			playerPrefs.Edit ().PutBoolean (key, value).Commit();
		}

		public static bool GetBool (string key)
		{
			return playerPrefs.GetBoolean (key,false);
		}

		public static bool GetBool (string key, bool defValue)
		{
			return playerPrefs.GetBoolean (key, defValue);
		}


		public static void Remove (string key)
		{
			playerPrefs.Edit().Remove(key).Commit();
		}
	}
}

