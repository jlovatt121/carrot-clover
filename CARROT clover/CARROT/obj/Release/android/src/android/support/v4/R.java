/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package android.support.v4;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static int carrot_dark_green=0x7f040004;
        public static int carrot_green=0x7f040003;
        public static int carrot_grey=0x7f040006;
        public static int carrot_light_grey=0x7f040007;
        public static int carrot_light_orange=0x7f040002;
        public static int carrot_millie_grey=0x7f040005;
        public static int carrot_orange=0x7f040000;
        public static int carrot_orange2=0x7f040001;
    }
    public static final class drawable {
        public static int carrot_top=0x7f020000;
        public static int icon=0x7f020001;
        public static int splash_screen=0x7f020002;
    }
    public static final class id {
        public static int Main_fragFrame=0x7f060006;
        public static int Main_header=0x7f060004;
        public static int imageView1=0x7f060001;
        public static int linearLayout1=0x7f060003;
        public static int progressBar1=0x7f060002;
        public static int relativeLayout1=0x7f060000;
        public static int textView1=0x7f060005;
    }
    public static final class layout {
        public static int loadingactivitylayout=0x7f030000;
        public static int mainactivitylayout=0x7f030001;
    }
    public static final class string {
        public static int app_name=0x7f050001;
        public static int hello=0x7f050000;
    }
}
