package mono.com.clover.sdk.v1;


public class ServiceConnector_OnServiceConnectedListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.clover.sdk.v1.ServiceConnector.OnServiceConnectedListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onServiceConnected:(Lcom/clover/sdk/v1/ServiceConnector;)V:GetOnServiceConnected_Lcom_clover_sdk_v1_ServiceConnector_Handler:Com.Clover.Sdk.V1.ServiceConnector/IOnServiceConnectedListenerInvoker, BindingTest\n" +
			"n_onServiceDisconnected:(Lcom/clover/sdk/v1/ServiceConnector;)V:GetOnServiceDisconnected_Lcom_clover_sdk_v1_ServiceConnector_Handler:Com.Clover.Sdk.V1.ServiceConnector/IOnServiceConnectedListenerInvoker, BindingTest\n" +
			"";
		mono.android.Runtime.register ("Com.Clover.Sdk.V1.ServiceConnector+IOnServiceConnectedListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ServiceConnector_OnServiceConnectedListenerImplementor.class, __md_methods);
	}


	public ServiceConnector_OnServiceConnectedListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ServiceConnector_OnServiceConnectedListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Clover.Sdk.V1.ServiceConnector+IOnServiceConnectedListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onServiceConnected (com.clover.sdk.v1.ServiceConnector p0)
	{
		n_onServiceConnected (p0);
	}

	private native void n_onServiceConnected (com.clover.sdk.v1.ServiceConnector p0);


	public void onServiceDisconnected (com.clover.sdk.v1.ServiceConnector p0)
	{
		n_onServiceDisconnected (p0);
	}

	private native void n_onServiceDisconnected (com.clover.sdk.v1.ServiceConnector p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
