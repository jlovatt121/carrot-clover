package mono.com.clover.sdk.v3.order;


public class OrderConnector_OnOrderUpdateListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.clover.sdk.v3.order.OrderConnector.OnOrderUpdateListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onOrderUpdated:(Ljava/lang/String;Z)V:GetOnOrderUpdated_Ljava_lang_String_ZHandler:Com.Clover.Sdk.V3.Order.OrderConnector/IOnOrderUpdateListenerInvoker, BindingTest\n" +
			"";
		mono.android.Runtime.register ("Com.Clover.Sdk.V3.Order.OrderConnector+IOnOrderUpdateListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", OrderConnector_OnOrderUpdateListenerImplementor.class, __md_methods);
	}


	public OrderConnector_OnOrderUpdateListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == OrderConnector_OnOrderUpdateListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Clover.Sdk.V3.Order.OrderConnector+IOnOrderUpdateListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onOrderUpdated (java.lang.String p0, boolean p1)
	{
		n_onOrderUpdated (p0, p1);
	}

	private native void n_onOrderUpdated (java.lang.String p0, boolean p1);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
