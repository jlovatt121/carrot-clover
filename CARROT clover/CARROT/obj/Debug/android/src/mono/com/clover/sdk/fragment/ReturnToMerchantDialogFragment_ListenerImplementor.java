package mono.com.clover.sdk.fragment;


public class ReturnToMerchantDialogFragment_ListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.clover.sdk.fragment.ReturnToMerchantDialogFragment.Listener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onDismiss:()V:GetOnDismissHandler:Com.Clover.Sdk.Fragment.ReturnToMerchantDialogFragment/IListenerInvoker, BindingTest\n" +
			"";
		mono.android.Runtime.register ("Com.Clover.Sdk.Fragment.ReturnToMerchantDialogFragment+IListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ReturnToMerchantDialogFragment_ListenerImplementor.class, __md_methods);
	}


	public ReturnToMerchantDialogFragment_ListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ReturnToMerchantDialogFragment_ListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Clover.Sdk.Fragment.ReturnToMerchantDialogFragment+IListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onDismiss ()
	{
		n_onDismiss ();
	}

	private native void n_onDismiss ();

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
