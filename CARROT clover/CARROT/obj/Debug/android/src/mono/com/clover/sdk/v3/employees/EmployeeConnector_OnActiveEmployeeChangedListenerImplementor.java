package mono.com.clover.sdk.v3.employees;


public class EmployeeConnector_OnActiveEmployeeChangedListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.clover.sdk.v3.employees.EmployeeConnector.OnActiveEmployeeChangedListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onActiveEmployeeChanged:(Lcom/clover/sdk/v3/employees/Employee;)V:GetOnActiveEmployeeChanged_Lcom_clover_sdk_v3_employees_Employee_Handler:Com.Clover.Sdk.V3.Employees.EmployeeConnector/IOnActiveEmployeeChangedListenerInvoker, BindingTest\n" +
			"";
		mono.android.Runtime.register ("Com.Clover.Sdk.V3.Employees.EmployeeConnector+IOnActiveEmployeeChangedListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", EmployeeConnector_OnActiveEmployeeChangedListenerImplementor.class, __md_methods);
	}


	public EmployeeConnector_OnActiveEmployeeChangedListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == EmployeeConnector_OnActiveEmployeeChangedListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Clover.Sdk.V3.Employees.EmployeeConnector+IOnActiveEmployeeChangedListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onActiveEmployeeChanged (com.clover.sdk.v3.employees.Employee p0)
	{
		n_onActiveEmployeeChanged (p0);
	}

	private native void n_onActiveEmployeeChanged (com.clover.sdk.v3.employees.Employee p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
