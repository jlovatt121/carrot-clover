package mono.com.clover.sdk.v1.merchant;


public class MerchantConnector_OnMerchantChangedListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.clover.sdk.v1.merchant.MerchantConnector.OnMerchantChangedListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onMerchantChanged:(Lcom/clover/sdk/v1/merchant/Merchant;)V:GetOnMerchantChanged_Lcom_clover_sdk_v1_merchant_Merchant_Handler:Com.Clover.Sdk.V1.Merchant.MerchantConnector/IOnMerchantChangedListenerInvoker, BindingTest\n" +
			"";
		mono.android.Runtime.register ("Com.Clover.Sdk.V1.Merchant.MerchantConnector+IOnMerchantChangedListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MerchantConnector_OnMerchantChangedListenerImplementor.class, __md_methods);
	}


	public MerchantConnector_OnMerchantChangedListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MerchantConnector_OnMerchantChangedListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Clover.Sdk.V1.Merchant.MerchantConnector+IOnMerchantChangedListenerImplementor, BindingTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onMerchantChanged (com.clover.sdk.v1.merchant.Merchant p0)
	{
		n_onMerchantChanged (p0);
	}

	private native void n_onMerchantChanged (com.clover.sdk.v1.merchant.Merchant p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
