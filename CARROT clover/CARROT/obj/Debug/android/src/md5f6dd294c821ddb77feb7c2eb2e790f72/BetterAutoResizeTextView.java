package md5f6dd294c821ddb77feb7c2eb2e790f72;


public class BetterAutoResizeTextView
	extends md5f6dd294c821ddb77feb7c2eb2e790f72.BetterTextView
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onMeasure:(II)V:GetOnMeasure_IIHandler\n" +
			"n_onTextChanged:(Ljava/lang/CharSequence;III)V:GetOnTextChanged_Ljava_lang_CharSequence_IIIHandler\n" +
			"n_getTextSize:()F:GetGetTextSizeHandler\n" +
			"n_setTextSize:(F)V:GetSetTextSize_FHandler\n" +
			"n_onSizeChanged:(IIII)V:GetOnSizeChanged_IIIIHandler\n" +
			"";
		mono.android.Runtime.register ("CarrotClover.BetterAutoResizeTextView, CARROT, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", BetterAutoResizeTextView.class, __md_methods);
	}


	public BetterAutoResizeTextView (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == BetterAutoResizeTextView.class)
			mono.android.TypeManager.Activate ("CarrotClover.BetterAutoResizeTextView, CARROT, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public BetterAutoResizeTextView (android.content.Context p0, android.util.AttributeSet p1) throws java.lang.Throwable
	{
		super (p0, p1);
		if (getClass () == BetterAutoResizeTextView.class)
			mono.android.TypeManager.Activate ("CarrotClover.BetterAutoResizeTextView, CARROT, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public BetterAutoResizeTextView (android.content.Context p0, android.util.AttributeSet p1, int p2) throws java.lang.Throwable
	{
		super (p0, p1, p2);
		if (getClass () == BetterAutoResizeTextView.class)
			mono.android.TypeManager.Activate ("CarrotClover.BetterAutoResizeTextView, CARROT, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public void onMeasure (int p0, int p1)
	{
		n_onMeasure (p0, p1);
	}

	private native void n_onMeasure (int p0, int p1);


	public void onTextChanged (java.lang.CharSequence p0, int p1, int p2, int p3)
	{
		n_onTextChanged (p0, p1, p2, p3);
	}

	private native void n_onTextChanged (java.lang.CharSequence p0, int p1, int p2, int p3);


	public float getTextSize ()
	{
		return n_getTextSize ();
	}

	private native float n_getTextSize ();


	public void setTextSize (float p0)
	{
		n_setTextSize (p0);
	}

	private native void n_setTextSize (float p0);


	public void onSizeChanged (int p0, int p1, int p2, int p3)
	{
		n_onSizeChanged (p0, p1, p2, p3);
	}

	private native void n_onSizeChanged (int p0, int p1, int p2, int p3);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
