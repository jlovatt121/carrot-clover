package md5f6dd294c821ddb77feb7c2eb2e790f72;


public class BaseActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("CarrotClover.BaseActivity, CARROT, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", BaseActivity.class, __md_methods);
	}


	public BaseActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == BaseActivity.class)
			mono.android.TypeManager.Activate ("CarrotClover.BaseActivity, CARROT, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
