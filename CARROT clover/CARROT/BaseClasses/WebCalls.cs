﻿using System;
using System.Net;
using System.IO;

namespace CarrotClover
{
	public static class WebCalls
	{
		private static readonly object thisLock = new object();

		public static void MakeAuthWebCall (string url, string method, string postString, Action<WebAnswer> callback)
		{
			lock (thisLock)
			{
				ConsolePrime.WriteLine ("Make Auth Webcall: " + url);

				WebRequest request = WebRequest.Create (url);
				request.Method = method;
				request.ContentType = "application/x-www-form-urlencoded";
				request.Headers.Add ("cage", "carrotApp");
				request.Headers.Add ("connery", "FfktyvyHszvnaWPEhOP3w-AyphVBlmQ1");
				request.Headers.Add ("userTicket", User.Ticket);
				request.Headers.Add ("appPlatform", "Clover");
				request.Headers.Add ("versionNum", User.AppVersion);

				try
				{
					if (!string.IsNullOrWhiteSpace(postString))
						using (StreamWriter writer = new StreamWriter (request.GetRequestStream()))
							writer.Write (postString);

					using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
					{
						using (StreamReader reader = new StreamReader(response.GetResponseStream()))
						{
							string answer = reader.ReadToEnd();

							ConsolePrime.WriteLine ("Non Catch Answer: " + answer);
							WebAnswer currentAnswer = new WebAnswer (response.StatusCode, response.Headers, answer);
							callback (currentAnswer);
						}
					}
						
					
				}
				catch (WebException exception)
				{
					using (HttpWebResponse response = (HttpWebResponse)exception.Response)
					{
						if (response != null && response.StatusCode != null && response.Headers != null && exception.Message != null)
							callback (new WebAnswer (response.StatusCode, response.Headers, exception.Message));
						else callback (new WebAnswer (HttpStatusCode.BadRequest, null, ""));
					}
				}
			}
		}


	}
}

