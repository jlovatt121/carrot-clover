﻿using System;
using System.ComponentModel;

using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Graphics;
using Android.Media.Audiofx;
using Android.Media;
using Android.Util;

namespace CarrotClover
{
	public class RobotoButton : Button
	{
		public RobotoButton (Context context) : base (context)
		{
			Init ();
		}

		public RobotoButton (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public RobotoButton (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public RobotoButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class RobotoBetterButton : Button
	{
		ColorFilter currentColorFilter;
		public RobotoBetterButton (Context context) : base (context)
		{
			Init ();
		}

		public RobotoBetterButton (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public RobotoBetterButton (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public RobotoBetterButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			base.OnTouchEvent (e);

			switch (e.Action)
			{
				case MotionEventActions.Down:
					if (this.Clickable)
					{
						this.currentColorFilter = null;
						this.Background.SetColorFilter (new Color (0x77000000), PorterDuff.Mode.SrcAtop);
						this.Invalidate ();
					}
					break;
				case MotionEventActions.Up:
				case MotionEventActions.Cancel:
					if (this.Clickable)
					{
						this.Background.ClearColorFilter ();						
						this.Invalidate ();
					}
					break;
			}
			return true;
		}
	}

	public class RobotoEditText : EditText
	{
		Context context;
		TextView textView;
		public RobotoEditText (Context context) : base (context)
		{
			Init ();
		}

		public RobotoEditText (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public RobotoEditText (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public RobotoEditText (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			if (context != null)
			{
				textView = new TextView (context);
				var f = textView.Class.GetDeclaredField ("mCursorDrawableRes");
				f.Accessible = true;
				f.Set (this, 0);
			}

			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class RobotoEditTextWhiteLine : WhiteLineEditText
	{
		Context context;
		TextView textView;
		public RobotoEditTextWhiteLine (Context context) : base (context)
		{
			Init ();
		}

		public RobotoEditTextWhiteLine (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public RobotoEditTextWhiteLine (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public RobotoEditTextWhiteLine (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			if (context != null)
			{
				textView = new TextView (context);
				var f = textView.Class.GetDeclaredField ("mCursorDrawableRes");
				f.Accessible = true;
				f.Set (this, 0);
			}

			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class RobotoAutoResizeTextView : NoFontAutoResizeTextView
	{
		public RobotoAutoResizeTextView (Context context) : base (context)
		{
			Init ();
		}

		public RobotoAutoResizeTextView (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public RobotoAutoResizeTextView (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public RobotoAutoResizeTextView (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}		



	public class RobotoTextView : TextView
	{
		public RobotoTextView (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle)
		{
			Init ();
		}

		public RobotoTextView (Context context, IAttributeSet attrs) : base (context, attrs)
		{
			Init ();
		}

		public RobotoTextView (Context context) : base (context)
		{
			Init ();
		}

		public RobotoTextView (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class RobotoRadioButton : RadioButton
	{
		public RobotoRadioButton (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle)
		{
			Init ();
		}

		public RobotoRadioButton (Context context, IAttributeSet attrs) : base (context, attrs)
		{
			Init ();
		}

		public RobotoRadioButton (Context context) : base (context)
		{
			Init ();
		}

		public RobotoRadioButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Roboto-Light.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}
}

