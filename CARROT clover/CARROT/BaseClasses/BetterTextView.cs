﻿using System;

using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using Android.Runtime;

namespace CarrotClover
{
	public class BetterTextView : TextView
	{
		public BetterTextView (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle)
		{
			Init ();
		}

		public BetterTextView (Context context, IAttributeSet attrs) : base (context, attrs)
		{
			Init ();
		}

		public BetterTextView (Context context) : base (context)
		{
			Init ();
		}

		public BetterTextView (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}
}

