﻿using System;
using System.Collections;
using System.Collections.Generic;

using Android.Graphics;

using Com.Clover.Sdk.V3.Order;

namespace CarrotClover
{
	public interface ICloverDiscount
	{
		Discount GetDiscount ();
	}

	public enum DiscountType
	{
		PercentageDiscount,
		AmountDiscount,
		BuyXGetXDiscount
	}

	public class PercentageDiscount : ICloverDiscount
	{
		public DiscountType Type = DiscountType.PercentageDiscount;

		public string discountName = "";

		public long percentageOff = 0;

		public string lineItemID = "-1";


		public PercentageDiscount (string DiscountName, long PercentageOff, string LineItemID = "")
		{
			this.discountName = DiscountName;
			this.percentageOff = PercentageOff;
			if (!string.IsNullOrWhiteSpace (LineItemID))
				this.lineItemID = LineItemID;
		}

		public Discount GetDiscount ()
		{
			Discount thisDiscount = new Discount ();
			thisDiscount.SetName (this.discountName);
			thisDiscount.SetPercentage (Java.Lang.Long.ValueOf (percentageOff));

			return thisDiscount;
		}
	}

	public class AmountDiscount : ICloverDiscount
	{
		public DiscountType Type = DiscountType.AmountDiscount;

		public string discountName = "";

		/// <summary>
		/// The amount off in cents
		/// </summary>
		public long amountOff = 0;

		public string lineItemID = "-1";

		/// <summary>
		/// Initializes a new instance of the <see cref="CarrotClover.AmountDiscount"/> class.
		/// </summary>
		/// <param name="DiscountName">Discount name.</param>
		/// <param name="AmountOff">Amount off in cents.</param>
		/// <param name="LineItemID">Line item ID.</param>
		public AmountDiscount (string DiscountName, long AmountOff, string LineItemID = "")
		{
			this.discountName = DiscountName;
			this.amountOff = AmountOff;
			if (!string.IsNullOrWhiteSpace (LineItemID))
				this.lineItemID = LineItemID;
		}

		public Discount GetDiscount ()
		{
			Discount thisDiscount = new Discount ();
			thisDiscount.SetName (this.discountName);
			thisDiscount.SetAmount (Java.Lang.Long.ValueOf (-amountOff));

			return thisDiscount;
		}
	}

	public class BuyXGetXDiscount : ICloverDiscount
	{
		public DiscountType Type = DiscountType.BuyXGetXDiscount;

		public string discountName = "";

		public BuyXGetXDiscount ()
		{

		}

		public Discount GetDiscount ()
		{
			return null;
		}
	}




}

