﻿using System;
using System.ComponentModel;

using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Graphics;
using Android.Media.Audiofx;
using Android.Media;

namespace CarrotClover
{
	public class BetterButton : Button
	{
		ColorFilter currentColorFilter;
		public BetterButton (Context context) : base (context)
		{
			Init ();
		}

		public BetterButton (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public BetterButton (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public BetterButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			base.OnTouchEvent (e);

			switch (e.Action)
			{
				case MotionEventActions.Down:
					if (this.Clickable)
					{
						this.currentColorFilter = null;
						this.Background.SetColorFilter (new Color (0x77000000), PorterDuff.Mode.SrcAtop);
						this.Invalidate ();
					}
					break;
				case MotionEventActions.Up:
				case MotionEventActions.Cancel:
					if (this.Clickable)
					{
						this.Background.ClearColorFilter ();						
						this.Invalidate ();
					}
					break;
			}
			return true;
		}
	}

	public class BetterImageButton : ImageButton
	{
		ColorFilter currentColorFilter;
		public BetterImageButton (Context context) : base (context)
		{
			Init ();
		}

		public BetterImageButton (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public BetterImageButton (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public BetterImageButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			base.OnTouchEvent (e);

			switch (e.Action)
			{
				case MotionEventActions.Down:
					if (this.Clickable)
					{
						this.currentColorFilter = null;
						this.SetColorFilter (new Color (0x77000000), PorterDuff.Mode.SrcAtop);
//						this.Background.SetColorFilter (new Color (0x77000000), PorterDuff.Mode.SrcAtop);
						this.Invalidate ();
					}
					break;
				case MotionEventActions.Up:
				case MotionEventActions.Cancel:
					if (this.Clickable)
					{
						this.ClearColorFilter ();
//						this.Background.ClearColorFilter ();						
						this.Invalidate ();
					}
					break;
			}
			return true;
		}
	}
}

