﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Threading;

using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware;
using Android.Support.V4.App;

namespace CarrotClover
{
	public static class Alerts
	{
		public static void PresentAlert (Context context, string title, string message)
		{
			Activity activ = (Activity)context;

			activ.RunOnUiThread (() =>
			{
				AlertDialog alert = new AlertDialog.Builder (activ).SetTitle (title).SetMessage (message).SetNeutralButton ("OK", (s, e) =>
				{
				}).Create ();
				alert.SetCanceledOnTouchOutside (false);
				alert.SetCancelable (false);
				activ.RunOnUiThread (() =>
				{
					alert.Show ();
				});
			});
		}

		public static void PresentAlert (Context context, string title, string message, Action okHandler)
		{
			Activity activ = (Activity)context;

			activ.RunOnUiThread (() =>
			{
				AlertDialog alert = new AlertDialog.Builder (activ).SetTitle (title).SetMessage (message).SetNeutralButton ("OK", (s, e) =>
				{
					okHandler();
				}).Create ();
				alert.SetCanceledOnTouchOutside (false);
				alert.SetCancelable (false);
				activ.RunOnUiThread (() =>
				{
					alert.Show ();
				});
			});
		}
	}
}

