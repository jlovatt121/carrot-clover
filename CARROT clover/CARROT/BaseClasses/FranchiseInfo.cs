﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware;

namespace CarrotClover
{
	public class FranchiseInfo
	{
		public static List<FranchiseInfo> AllMyFranchises = new List<FranchiseInfo> ();
		public static List<FranchiseInfo> CouponEligableFranchises = new List<FranchiseInfo> ();
		public static FranchiseInfo selectedFranchise;

		public long franID = -1;
		public string name;
		public string phoneNum;
		public double latitude;
		public double longitude;
		public string city;
		public string state;
		public string country;
		public string address;
		public string zip;
		public string timeZone;
		public string website;
		public string opHours;
		public string googPage;
		public string fbPage;
		public string twitPage;
		public string yelpPage;
		public long distCounter = -1;
		public long distDoor = -1;
		public string majorMinor;
		public List<Coupon> offers = new List<Coupon>();
		public Coupon activeOfferCoup = new Coupon();
		public long requestAmount = -1;
		public bool isOwner;
		public bool isEditor;
		public bool isVerified;
		public bool hasBeacons;
		public string CurrPrimeLabel;
		public string CurrPrimeValue;
		public string VerifyToken;
		public string BeaconPIN;
		public bool hasSub;
		public string subExpireTime;
		public DateTime subExpireDate;
		public bool hasCarrotPage;
		public string activeOfferLabel;
		public bool isCommunity;
		public string category;
		public List<string> subCategories = new List<string>();
		public long likes = -1;
		public long numOffers = -1;
		public long numRewardOffers = -1;
		public double distFromMeMeters = -1;
		public string relevantText;
		public string currentOfferCombo;
		public string franDescription;
		public long numOtherOffers = 0;

		public FranchiseInfo ()
		{
		}

		public FranchiseInfo (Dictionary<string, object> FranchiseData)
		{
			UpdateFranchiseInfo (FranchiseData);
		}

		public void UpdateFranchiseInfo (Dictionary<string, object> FranchiseData)
		{
			if (FranchiseData.ContainsKey ("name") && FranchiseData["name"] != null) this.name = FranchiseData ["name"].ToString();
			if (FranchiseData.ContainsKey ("franID") && FranchiseData["franID"] != null) this.franID = long.Parse(FranchiseData ["franID"].ToString());
			if (FranchiseData.ContainsKey ("phoneNum") && FranchiseData["phoneNum"] != null) this.phoneNum = FranchiseData ["phoneNum"].ToString();
			if (FranchiseData.ContainsKey ("lat") && FranchiseData["lat"] != null) this.latitude = double.Parse (FranchiseData ["lat"].ToString());
			else if (FranchiseData.ContainsKey ("latitude") && FranchiseData ["latitude"] != null) this.latitude = double.Parse (FranchiseData ["latitude"].ToString ());
			if (FranchiseData.ContainsKey ("longi") && FranchiseData["longi"] != null) this.longitude = double.Parse (FranchiseData ["longi"].ToString());
			else if (FranchiseData.ContainsKey ("longitude") && FranchiseData ["longitude"] != null) this.longitude = double.Parse (FranchiseData ["longitude"].ToString ());
			if (FranchiseData.ContainsKey ("city") && FranchiseData["city"] != null) this.city = FranchiseData ["city"].ToString();
			if (FranchiseData.ContainsKey ("state") && FranchiseData["state"] != null) this.state = FranchiseData ["state"].ToString();
			if (FranchiseData.ContainsKey ("zip") && FranchiseData["zip"] != null) this.zip = FranchiseData ["zip"].ToString();
			if (FranchiseData.ContainsKey ("country") && FranchiseData["country"] != null) this.country = FranchiseData ["country"].ToString();
			if (FranchiseData.ContainsKey ("address") && FranchiseData["address"] != null) this.address = FranchiseData ["address"].ToString();
			if (FranchiseData.ContainsKey ("website") && FranchiseData["website"] != null) this.website = FranchiseData ["website"].ToString();
			if (FranchiseData.ContainsKey ("opHours") && FranchiseData["opHours"] != null) this.opHours = FranchiseData ["opHours"].ToString();
			if (FranchiseData.ContainsKey ("googPage") && FranchiseData["googPage"] != null) this.googPage = FranchiseData ["googPage"].ToString();
			if (FranchiseData.ContainsKey ("fbPage") && FranchiseData["fbPage"] != null) this.fbPage = FranchiseData ["fbPage"].ToString();
			if (FranchiseData.ContainsKey ("twitPage") && FranchiseData["twitPage"] != null) this.twitPage = FranchiseData ["twitPage"].ToString();
			if (FranchiseData.ContainsKey ("yelpPage") && FranchiseData["yelpPage"] != null) this.yelpPage = FranchiseData ["yelpPage"].ToString();
			if (FranchiseData.ContainsKey ("majorMinor") && FranchiseData["majorMinor"] != null) this.majorMinor = FranchiseData ["majorMinor"].ToString();
			if (FranchiseData.ContainsKey ("timeZone") && FranchiseData["timeZone"] != null) this.timeZone = FranchiseData ["timeZone"].ToString();
			if (FranchiseData.ContainsKey ("numAdminRequests") && FranchiseData["numAdminRequests"] != null) this.requestAmount = long.Parse(FranchiseData ["numAdminRequests"].ToString());
			if (FranchiseData.ContainsKey ("isOwner") && FranchiseData["isOwner"] != null) this.isOwner = bool.Parse(FranchiseData ["isOwner"].ToString());
			if (FranchiseData.ContainsKey ("isEditor") && FranchiseData["isEditor"] != null) this.isEditor = bool.Parse(FranchiseData ["isEditor"].ToString());
			if (FranchiseData.ContainsKey ("isVerified") && FranchiseData["isVerified"] != null) this.isVerified = bool.Parse(FranchiseData ["isVerified"].ToString());
			if (FranchiseData.ContainsKey ("hasBeacons") && FranchiseData["hasBeacons"] != null) this.hasBeacons = bool.Parse(FranchiseData ["hasBeacons"].ToString());
			if (FranchiseData.ContainsKey ("verificationToken") && FranchiseData["verificationToken"] != null) this.VerifyToken = FranchiseData ["verificationToken"].ToString();
			if (FranchiseData.ContainsKey ("beaconPIN") && FranchiseData["beaconPIN"] != null) this.BeaconPIN = FranchiseData ["beaconPIN"].ToString();
			if (FranchiseData.ContainsKey ("subActive") && FranchiseData["subActive"] != null) this.hasSub = bool.Parse(FranchiseData ["subActive"].ToString());
			if (FranchiseData.ContainsKey ("localActiveUntil") && FranchiseData ["localActiveUntil"] != null) this.subExpireTime = FranchiseData ["localActiveUntil"].ToString();
			if (FranchiseData.ContainsKey ("activeOfferLabel") && FranchiseData ["activeOfferLabel"] != null)
			{
				this.activeOfferLabel = FranchiseData ["activeOfferLabel"].ToString ();
			}
			else this.activeOfferLabel = "No Offers";
			if (FranchiseData.ContainsKey ("subCategories") && FranchiseData ["subCategories"] != null)
			{
				JArray a = FranchiseData ["subCategories"] as JArray;
				this.subCategories = a.ToObject<List<string>> ();
			}
			if (FranchiseData.ContainsKey ("numOtherOffers") && FranchiseData ["numOtherOffers"] != null) this.numOtherOffers = long.Parse (FranchiseData ["numOtherOffers"].ToString ());
			if (FranchiseData.ContainsKey ("numLikes") && FranchiseData ["numLikes"] != null) this.likes = long.Parse (FranchiseData ["numLikes"].ToString ());
			else if (FranchiseData.ContainsKey ("likes") && FranchiseData ["likes"] != null) this.likes = long.Parse (FranchiseData ["likes"].ToString ());
			if (FranchiseData.ContainsKey ("totalNumActiveOffers") && FranchiseData ["totalNumActiveOffers"] != null) this.numOffers = long.Parse (FranchiseData ["totalNumActiveOffers"].ToString ());
			if (FranchiseData.ContainsKey ("numActiveRewardOffers") && FranchiseData ["numActiveRewardOffers"] != null) this.numRewardOffers = long.Parse (FranchiseData ["numActiveRewardOffers"].ToString ());
			if (FranchiseData.ContainsKey ("isCommunity") && FranchiseData ["isCommunity"] != null) this.isCommunity = bool.Parse (FranchiseData ["isCommunity"].ToString ());
			if (FranchiseData.ContainsKey ("distanceFromCenter") && FranchiseData ["distanceFromCenter"] != null) this.distFromMeMeters = double.Parse (FranchiseData ["distanceFromCenter"].ToString ());
		}

		public void UpdateFranchiseFromServer ()
		{
			if (this.franID > -1)
			{
				string version;
				string appPlatform;

				var pInfo = Application.Context.PackageManager.GetPackageInfo (Application.Context.PackageName, 0);
				version = pInfo.VersionName;

				appPlatform = "Android";
				WebRequest request = WebRequest.Create ("https://carrot.azurewebsites.net/getfranchiseinfoforuser.aspx");
				request.Method = "POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Headers.Add ("cage", "carrotApp");
				request.Headers.Add ("connery", "FfktyvyHszvnaWPEhOP3w-AyphVBlmQ1");
				request.Headers.Add ("appPlatform", appPlatform);
				request.Headers.Add ("versionNum", version);

				string postString = "franID=" + this.franID;

				try
				{
					using (StreamWriter writer = new StreamWriter (request.GetRequestStream ()))
						writer.Write (postString);

					using (HttpWebResponse response = (HttpWebResponse)request.GetResponse ())
					{
						using (StreamReader reader = new StreamReader (response.GetResponseStream ()))
						{
							string answer = reader.ReadToEnd ();
							if (response.StatusCode == HttpStatusCode.Accepted)
							{
								var jsonDict = JsonConvert.DeserializeObject<Dictionary<string,object>> (answer);
								this.UpdateFranchiseInfo (jsonDict);
							}
						}
					}
				}
				catch
				{
				}
			}

		}

		public static void GetAllFranchises ()
		{			
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/getfranchiseinfo.aspx", "GET", "",(thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == System.Net.HttpStatusCode.Accepted)
				{
					AllMyFranchises.Clear();
					JArray a = JArray.Parse (answer);
					foreach (JObject o in a.Children<JObject>())
					{
						var jsonDict = JsonConvert.DeserializeObject<Dictionary<string,object>> (o.ToString());

						FranchiseInfo newFran = new FranchiseInfo(jsonDict);
						AllMyFranchises.Add(newFran);

						ConsolePrime.WriteLine ("Found Franchise");
						ConsolePrime.WriteLine (newFran.name);
						ConsolePrime.WriteLine (newFran.latitude.ToString());
						ConsolePrime.WriteLine (newFran.longitude.ToString());
					}
				}
			});
		}

		public static void GetFranchisesEligableForCoupons ()
		{
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/getfranchiseseligibleforoffercreation.aspx", "GET", "",(thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == System.Net.HttpStatusCode.Accepted)
				{
					CouponEligableFranchises.Clear();
					JArray a = JArray.Parse (answer);
					foreach (JObject o in a.Children<JObject>())
					{
						var jsonDict = JsonConvert.DeserializeObject<Dictionary<string,object>> (o.ToString());

						FranchiseInfo newFran = new FranchiseInfo(jsonDict);
						newFran.isEditor = true;
						CouponEligableFranchises.Add(newFran);

						ConsolePrime.WriteLine ("Found Franchise");
						ConsolePrime.WriteLine (newFran.name);
						ConsolePrime.WriteLine (newFran.latitude.ToString());
						ConsolePrime.WriteLine (newFran.longitude.ToString());
					}
				}
			});
		}
	}
}

