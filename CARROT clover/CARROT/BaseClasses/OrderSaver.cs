﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CarrotClover
{			
	public class OrderSaver
	{
		private static Dictionary<string,List<string>>  OrderFranLinker = new Dictionary<string,List<string>>();

		public static void LinkBarcodeToOrder (string orderID, string barcode)
		{
			if (!OrderFranLinker.ContainsKey (orderID))
				OrderFranLinker.Add (orderID, new List<string> ());

			OrderFranLinker [orderID].Add (barcode);
		}

		public static void RemoveOrderLink (string orderID)
		{
			if (OrderFranLinker.ContainsKey (orderID))
				OrderFranLinker.Remove (orderID);
		}

		public static List<string> BarcodesForOrderID (string orderID)
		{
			List<string> barcodes = new List<string> ();

			if (OrderFranLinker.ContainsKey (orderID))
				barcodes = OrderFranLinker [orderID];

			return barcodes;
		}
	}
}

