﻿using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using Android.Runtime;


namespace CarrotClover
{
	/*
	public class NewAutoResizeTextView : TextView
	{
		public static float MIN_TEXT_SIZE = 26;

		public static float MAX_TEXT_SIZE = 128;

		private static int BISECTION_LOOP_WATCH_DOG = 30;

		public interface OnTextResizeListener
		{
			void OnTextResize (TextView textView, float oldSize, float newSiize);
		}

		private static string mEllipsis = "...";

		private OnTextResizeListener mTextResizeListener;

		private bool mNeedsResize = false;

		private float mTextSize;

		private float mMaxTextSize = MAX_TEXT_SIZE;

		private float mMinTextSize = MIN_TEXT_SIZE;

		private float mSpacingMult = 1.0f;

		private float mSpacingAdd = 0.0f;

		private bool mAddEllipsis = true;

		public NewAutoResizeTextView (Context context) : base (context) {
			Init ();
		}

		public NewAutoResizeTextView(Context context, IAttributeSet attrs) : base (context, attrs) {
			Init ();
		}

		public NewAutoResizeTextView (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle) {
			Init ();
		}

		public NewAutoResizeTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {
			Init ();
		}

		public void Init()
		{
			mTextSize = TextSize;
		}

		protected override void OnTextChanged (Java.Lang.ICharSequence text, int start, int lengthBefore, int lengthAfter)
		{
			mNeedsResize = true;
			ResetTextSize ();
		}

		protected override void OnSizeChanged (int w, int h, int oldw, int oldh)
		{
			if (w != oldw || h != oldh) {
				mNeedsResize = true;
			}
		}

		public void SetOnResizeListener (OnTextResizeListener listener)
		{
			mTextResizeListener = listener;
		}
			
		public override void SetTextSize (ComplexUnitType unit, float size)
		{
			base.SetTextSize (unit, size);
			mTextSize = TextSize;
		}

		public override void SetLineSpacing (float add, float mult)
		{
			base.SetLineSpacing (add, mult);
			mSpacingMult = mult;
			mSpacingAdd = add;
		}

		public void SetMaxTextSize (float maxTextSize)
		{
			mMaxTextSize = maxTextSize;
			RequestLayout ();
			Invalidate ();
		}

		public float GetMaxTextSize ()
		{
			return mMaxTextSize;
		}

		public void SetMinTextSize (float minTextSize)
		{
			mMinTextSize = minTextSize;
			RequestLayout ();
			Invalidate ();
		}

		public float GetMinTextSize()
		{
			return mMinTextSize;
		}

		public void SetAddEllipsis (bool addEllipsis)
		{
			mAddEllipsis = addEllipsis;
		}

		public bool GetAddEllipsis()
		{
			return mAddEllipsis;
		}

		public void ResetTextSize ()
		{
			if (mTextSize > 0)
				base.SetTextSize (ComplexUnitType.Px, mTextSize);
		}

		protected override void OnLayout (bool changed, int left, int top, int right, int bottom)
		{
			if (changed || mNeedsResize)
			{
				int widthLimit = (right - left) - CompoundPaddingLeft - CompoundPaddingRight;
				int heightLimit = (bottom - top) - CompoundPaddingBottom - CompoundPaddingTop;
				ResizeText (widthLimit, heightLimit);
			}
			base.OnLayout (changed, left, top, right, bottom);
		}

		public void ResizeText ()
		{
			int heightLimit = Height - PaddingBottom - PaddingTop;
			int widthLimit = Width - PaddingLeft - PaddingRight;
			ResizeText (widthLimit, heightLimit);
		}

		public void ResizeText (int width, int height)
		{
			string text = Text;

			if (text == null || text.Length == 0 || height <= 0 || width <= 0 || mTextSize == 0)
			{
				return;
			}

			var textPaint = this.Paint;

			float oldTextSize = textPaint.TextSize;

			float lower = mMinTextSize;
			float upper = mMaxTextSize;
			int loop_counter = 1;
			float targetTextSize = (lower + upper) / 2;
			int textHeight = GetTextHeight (text, textPaint, width, targetTextSize);
			while (loop_counter < BISECTION_LOOP_WATCH_DOG && upper - lower > 1)
			{
				targetTextSize = (lower + upper) / 2;
				textHeight = GetTextHeight (text, textPaint, width, targetTextSize);
				if (textHeight > height)
					upper = targetTextSize;
				else lower = targetTextSize;
				loop_counter++;
			}

			targetTextSize = lower;
			textHeight = GetTextHeight (text, textPaint, width, targetTextSize);

			if (mAddEllipsis && targetTextSize == mMinTextSize && textHeight > height)
			{
				var paintCopy = new Android.Text.TextPaint (textPaint);
				paintCopy.TextSize = targetTextSize;
				var layout = new Android.Text.StaticLayout (text, paintCopy, width, Android.Text.Layout.Alignment.AlignNormal, mSpacingMult, mSpacingAdd, false);
				if (layout.LineCount > 0)
				{
					int lastLine = layout.GetLineForVertical (height) - 1;
					if (lastLine < 0)
						Text = "";
					else
					{
						int start = layout.GetLineStart (lastLine);
						int end = layout.GetLineEnd (lastLine);
						float lineWidth = layout.GetLineWidth (lastLine);
						float ellipseWidth = paintCopy.MeasureText (mEllipsis);

						while (width < lineWidth + ellipseWidth)
							lineWidth = paintCopy.MeasureText (text.Substring (start, --end + 1));
						text = text.Substring (0, end) + mEllipsis;
					}
				}
			}

			SetTextSize (ComplexUnitType.Px, targetTextSize);
			SetLineSpacing (mSpacingAdd, mSpacingMult);

			if (mTextResizeListener != null)
				mTextResizeListener.OnTextResize (this, oldTextSize, targetTextSize);

			mNeedsResize = false;
		}

		private int GetTextHeight (string source, Android.Text.TextPaint originalPaint, int width, float textSize)
		{
			Android.Text.TextPaint paint = new Android.Text.TextPaint (originalPaint);
			paint.TextSize = textSize;
			Android.Text.StaticLayout layout = new Android.Text.StaticLayout (source, paint, width, Android.Text.Layout.Alignment.AlignNormal, mSpacingMult, mSpacingAdd, true);
			return layout.Height;
		}
	}

*/

	/// <summary>
	/// TextView that automatically resizes it's content to fit the layout dimensions
	/// </summary>
	/// <remarks>Port of: http://ankri.de/autoscale-textview/</remarks>
	public class AutoResizeTextView : TextView
	{
		/// <summary>
		/// How close we have to be to the perfect size
		/// </summary>
		private const float Threshold = .1f;

		/// <summary>
		/// Default minimum text size
		/// </summary>
		private const float DefaultMinTextSize = 10f;

		private Android.Text.TextPaint _textPaint;
		private float _preferredTextSize;
		private float initialTextSize;

		public AutoResizeTextView(Context context) : base(context)
		{
			Initialise(context, null);
		}

		public AutoResizeTextView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			Initialise(context, attrs);
		}

		public AutoResizeTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
		{
			Initialise(context, attrs);
		}

		// Default constructor override for MonoDroid
		public AutoResizeTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
			Initialise(null, null);
		}

		private void Initialise(Context context, IAttributeSet attrs)
		{
			_textPaint = new Android.Text.TextPaint();
			_textPaint.SetTypeface (Typeface.CreateFromAsset (Context.Assets, "Futura-CondensedMedium.ttf"));
			MinTextSize = DefaultMinTextSize;
			_preferredTextSize = TextSize;
			initialTextSize = TextSize;

			//			if (context != null && attrs != null)
			//			{
			//				//				//var attributes = context.ObtainStyledAttributes (attrs ,  Resource.Styleable.AutoResizeTextView);
			//				MinTextSize = DefaultMinTextSize;/*attributes.GetDimension (Resource.Styleable.AutoResizeTextView_minTextSize, DefaultMinTextSize);*/
			//				//				//attributes.Recycle();
			//				//
			//				_preferredTextSize = TextSize;
			//			}
		}

		/// <summary>
		/// Minimum text size in actual pixels
		/// </summary>
		public float MinTextSize { get; set; }

		/// <summary>
		/// Resize the text so that it fits.
		/// </summary>
		/// <param name="text">Text</param>
		/// <param name="textWidth">Width of the TextView</param>
		protected virtual void RefitText(string text, int textWidth)
		{
			if (textWidth <= 0 || string.IsNullOrWhiteSpace(text))
				return;

			var tempMinSize = MinTextSize;
			int targetWidth = textWidth - CompoundPaddingLeft - CompoundPaddingRight;
			_textPaint.Set(this.Paint);
			float size;
			while ((_preferredTextSize - tempMinSize) > Threshold)
			{
				size = (_preferredTextSize + tempMinSize) / 2f;
				_textPaint.TextSize = size;

				if ((_textPaint.MeasureText(text)) > targetWidth)
					_preferredTextSize = size; // Too big
				else
					tempMinSize = size; // Too small
			}

			SetTextSize(ComplexUnitType.Px, tempMinSize);
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
			int parentWidth = MeasureSpec.GetSize (widthMeasureSpec);
			int height = MeasuredHeight;
			RefitText (this.Text, parentWidth);
			this.SetMeasuredDimension (parentWidth, height);
		}

		protected override void OnTextChanged(Java.Lang.ICharSequence text, int start, int before, int after)
		{
			base.OnTextChanged(text, start, before, after);
			//			MinTextSize = DefaultMinTextSize;
			_preferredTextSize = initialTextSize;
			RefitText(text.ToString(), Width);
		}

		public override float TextSize
		{
			get {
				return base.TextSize;
			}
			set {
				base.TextSize = value;
				_preferredTextSize = value;
				initialTextSize = value;
			}
		}

		protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
		{
			base.OnSizeChanged(w, h, oldw, oldh);

			if (w != oldw)
				RefitText(Text, Width);
		}
	}

	public class NoFontAutoResizeTextView : TextView
	{
		/// <summary>
		/// How close we have to be to the perfect size
		/// </summary>
		private const float Threshold = .1f;

		/// <summary>
		/// Default minimum text size
		/// </summary>
		private const float DefaultMinTextSize = 10f;

		private Android.Text.TextPaint _textPaint;
		private float _preferredTextSize;
		private float initialTextSize;

		public NoFontAutoResizeTextView(Context context) : base(context)
		{
			Initialise(context, null);
		}

		public NoFontAutoResizeTextView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			Initialise(context, attrs);
		}

		public NoFontAutoResizeTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
		{
			Initialise(context, attrs);
		}

		// Default constructor override for MonoDroid
		public NoFontAutoResizeTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
			Initialise(null, null);
		}

		private void Initialise(Context context, IAttributeSet attrs)
		{
			_textPaint = new Android.Text.TextPaint();
			MinTextSize = DefaultMinTextSize;
			_preferredTextSize = TextSize;
			initialTextSize = TextSize;

			//			if (context != null && attrs != null)
			//			{
			//				//				//var attributes = context.ObtainStyledAttributes (attrs ,  Resource.Styleable.AutoResizeTextView);
			//				MinTextSize = DefaultMinTextSize;/*attributes.GetDimension (Resource.Styleable.AutoResizeTextView_minTextSize, DefaultMinTextSize);*/
			//				//				//attributes.Recycle();
			//				//
			//				_preferredTextSize = TextSize;
			//			}
		}

		/// <summary>
		/// Minimum text size in actual pixels
		/// </summary>
		public float MinTextSize { get; set; }

		/// <summary>
		/// Resize the text so that it fits.
		/// </summary>
		/// <param name="text">Text</param>
		/// <param name="textWidth">Width of the TextView</param>
		protected virtual void RefitText(string text, int textWidth)
		{
			if (textWidth <= 0 || string.IsNullOrWhiteSpace(text))
				return;

			var tempMinSize = MinTextSize;
			int targetWidth = textWidth - CompoundPaddingLeft - CompoundPaddingRight;
			_textPaint.Set(this.Paint);
			float size;
			while ((_preferredTextSize - tempMinSize) > Threshold)
			{
				size = (_preferredTextSize + tempMinSize) / 2f;
				_textPaint.TextSize = size;

				if ((_textPaint.MeasureText(text)) > targetWidth)
					_preferredTextSize = size; // Too big
				else
					tempMinSize = size; // Too small
			}

			SetTextSize(ComplexUnitType.Px, tempMinSize);
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
			int parentWidth = MeasureSpec.GetSize (widthMeasureSpec);
			int height = MeasuredHeight;
			RefitText (this.Text, parentWidth);
			this.SetMeasuredDimension (parentWidth, height);
		}

		protected override void OnTextChanged(Java.Lang.ICharSequence text, int start, int before, int after)
		{
			base.OnTextChanged(text, start, before, after);
			//			MinTextSize = DefaultMinTextSize;
			_preferredTextSize = initialTextSize;
			RefitText(text.ToString(), Width);
		}

		public override float TextSize
		{
			get {
				return base.TextSize;
			}
			set {
				base.TextSize = value;
				_preferredTextSize = value;
				initialTextSize = value;
			}
		}

		protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
		{
			base.OnSizeChanged(w, h, oldw, oldh);

			if (w != oldw)
				RefitText(Text, Width);
		}
	}

	/// <summary>
	/// TextView that automatically resizes it's content to fit the layout dimensions
	/// </summary>
	/// <remarks>Port of: http://ankri.de/autoscale-textview/</remarks>
	public class BetterAutoResizeTextView : BetterTextView
	{
		/// <summary>
		/// How close we have to be to the perfect size
		/// </summary>
		private const float Threshold = .1f;

		/// <summary>
		/// Default minimum text size
		/// </summary>
		private const float DefaultMinTextSize = 10f;

		private Android.Text.TextPaint _textPaint;
		private float _preferredTextSize;
		private float initialTextSize;

		public BetterAutoResizeTextView(Context context) : base(context)
		{
			Initialise(context, null);
		}

		public BetterAutoResizeTextView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			Initialise(context, attrs);
		}

		public BetterAutoResizeTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
		{
			Initialise(context, attrs);
		}

		// Default constructor override for MonoDroid
		public BetterAutoResizeTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
			Initialise(null, null);
		}

		private void Initialise(Context context, IAttributeSet attrs)
		{
			_textPaint = new Android.Text.TextPaint();
			_textPaint.SetTypeface (Typeface.CreateFromAsset (Context.Assets, "Futura-CondensedMedium.ttf"));
			MinTextSize = DefaultMinTextSize;
			_preferredTextSize = TextSize;
			initialTextSize = TextSize;

			//			if (context != null && attrs != null)
			//			{
			//				//				//var attributes = context.ObtainStyledAttributes (attrs ,  Resource.Styleable.AutoResizeTextView);
			//				MinTextSize = DefaultMinTextSize;/*attributes.GetDimension (Resource.Styleable.AutoResizeTextView_minTextSize, DefaultMinTextSize);*/
			//				//				//attributes.Recycle();
			//				//
			//				_preferredTextSize = TextSize;
			//			}
		}

		/// <summary>
		/// Minimum text size in actual pixels
		/// </summary>
		public float MinTextSize { get; set; }

		/// <summary>
		/// Resize the text so that it fits.
		/// </summary>
		/// <param name="text">Text</param>
		/// <param name="textWidth">Width of the TextView</param>
		protected virtual void RefitText(string text, int textWidth)
		{
			if (textWidth <= 0 || string.IsNullOrWhiteSpace(text))
				return;

			var tempMinSize = MinTextSize;
			int targetWidth = textWidth - CompoundPaddingLeft - CompoundPaddingRight;
			_textPaint.Set(this.Paint);
			float size;
			while ((_preferredTextSize - tempMinSize) > Threshold)
			{
				size = (_preferredTextSize + tempMinSize) / 2f;
				_textPaint.TextSize = size;

				if ((_textPaint.MeasureText(text)) > targetWidth)
					_preferredTextSize = size; // Too big
				else
					tempMinSize = size; // Too small
			}

			SetTextSize(ComplexUnitType.Px, tempMinSize);
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
			int parentWidth = MeasureSpec.GetSize (widthMeasureSpec);
			int height = MeasuredHeight;
			RefitText (this.Text, parentWidth);
			this.SetMeasuredDimension (parentWidth, height);
		}

		protected override void OnTextChanged(Java.Lang.ICharSequence text, int start, int before, int after)
		{
			base.OnTextChanged(text, start, before, after);
			//			MinTextSize = DefaultMinTextSize;
			_preferredTextSize = initialTextSize;
			RefitText(text.ToString(), Width);
		}

		public override float TextSize
		{
			get {
				return base.TextSize;
			}
			set {
				base.TextSize = value;
				_preferredTextSize = value;
				initialTextSize = value;
			}
		}

		protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
		{
			base.OnSizeChanged(w, h, oldw, oldh);

			if (w != oldw)
				RefitText(Text, Width);
		}
	}
}


namespace CarrotAndroid
{

	public class FontFitTextView : TextView
	{
		private Paint mTestPaint;

		public FontFitTextView(Context context) : base(context)
		{
			Initialise();
		}

		public FontFitTextView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			Initialise();
		}

		public FontFitTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
		{
			Initialise();
		}

		// Default constructor override for MonoDroid
		public FontFitTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
			Initialise();
		}

		private void Initialise()
		{
			mTestPaint = new Android.Graphics.Paint();
			mTestPaint.Set (this.Paint);
		}

		private void refitText (string text, int textWidth)
		{
			if (textWidth < 0) return;
			int targetWidth = textWidth - this.PaddingLeft - this.PaddingRight;
			float hi = 100;
			float lo = 2;
			float threshold = 0.5f;

			mTestPaint.Set (this.Paint);

			while ((hi - lo) > threshold)
			{
				float size = (hi + lo) / 2;
				mTestPaint.TextSize = size;
				if (mTestPaint.MeasureText (text) >= targetWidth)
					hi = size;
				else
					lo = size;

				this.SetTextSize (ComplexUnitType.Px, lo);
			}
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
			int parentWidth = MeasureSpec.GetSize (widthMeasureSpec);
			int height = this.MeasuredHeight;
			refitText (this.Text, parentWidth);
			this.SetMeasuredDimension (parentWidth, height);
		}

		protected override void OnTextChanged (Java.Lang.ICharSequence text, int start, int lengthBefore, int lengthAfter)
		{
			refitText (text.ToString (), this.Width);
		}

		protected override void OnSizeChanged (int w, int h, int oldw, int oldh)
		{
			if (w != oldw)
			{
				refitText (this.Text, w);
			}
		}
	}
}

