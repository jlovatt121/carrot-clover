﻿using System;

using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using Android.Runtime;
using Java.Lang;

namespace CarrotClover
{
	public class WhiteLineEditText : EditText
	{
		TextView textView;
		Context context;
		public WhiteLineEditText (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle)
		{
			this.context = context;
			Init ();
		}

		public WhiteLineEditText (Context context, IAttributeSet attrs) : base (context, attrs)
		{
			this.context = context;
			Init ();
		}

		public WhiteLineEditText (Context context) : base (context)
		{
			this.context = context;
			Init ();
		}

		public WhiteLineEditText (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			if (context != null)
			{
				textView = new TextView (context);
				var f = textView.Class.GetDeclaredField ("mCursorDrawableRes");
				f.Accessible = true;
				f.Set (this, 0);
			}

			this.SetBackgroundResource (Resource.Drawable.edittext_white_line_background);
		}

	}
}

