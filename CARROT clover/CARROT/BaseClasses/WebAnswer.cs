﻿using System;
using System.Net;
using System.Collections.Generic;

namespace CarrotClover
{
	public class WebAnswer
	{
		public HttpStatusCode statusCode;
		public WebHeaderCollection headers;
		public string answer = "";
		public Dictionary<string,object> data = new Dictionary<string, object>();

		public WebAnswer ()
		{
		}

		public WebAnswer (HttpStatusCode sc, WebHeaderCollection head)
		{
			this.statusCode = sc;
			this.headers = head;
		}

		public WebAnswer (HttpStatusCode sc, WebHeaderCollection head, string ans)
		{
			this.statusCode = sc;
			this.headers = head;
			this.answer = ans;
		}
	}
}

