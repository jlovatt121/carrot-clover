﻿using System;
using System.Collections;
using System.Collections.Generic;

using Android.Graphics;

using Newtonsoft.Json;

using Com.Clover.Sdk.V3.Order;

namespace CarrotClover
{
	public class Coupon
	{
		public static Coupon currentDivedCoupon;

		public bool Ovoided;
		public bool Cvoided;
		public string logoText;
		public string primLbl;
		public string primVal;
		public string localExpTime;
		public string localPushTime;
		public bool isExpired = false;
		public long numUses;
		public long numOverallUses;
		public long maxUses;
		public long totalUses;
		public long offerID;
		public string picLink;
		public string barcode;
		public bool isActive = false;
		public long pointsNeeded;
		public string twitterTime;
		public long maxUsesRemaining;
		public long totalUsesRemaining;
		public ICloverDiscount cloverDiscount;

		public Bitmap stripImage = null;
		public Bitmap qrImage = null;

		public Coupon ()
		{
		}

		public Coupon (Dictionary<string, object> jsonData)
		{
			UpdateCoupon (jsonData);
		}

		public void UpdateCoupon (Dictionary<string, object> jsonData)
		{
			if (jsonData.ContainsKey ("offerID") && jsonData ["offerID"] != null) this.offerID = long.Parse(jsonData ["offerID"].ToString ());

			if (jsonData.ContainsKey ("isActive") && jsonData ["isActive"] != null) this.isActive = bool.Parse (jsonData ["isActive"].ToString ());
			if (jsonData.ContainsKey ("stripLink") && jsonData ["stripLink"] != null) this.picLink = jsonData ["stripLink"].ToString ();
			if (jsonData.ContainsKey ("requiredPoints") && jsonData ["requiredPoints"] != null) this.pointsNeeded = long.Parse (jsonData ["requiredPoints"].ToString ());
			if (jsonData.ContainsKey ("primLabel") && jsonData ["primLabel"] != null) this.primLbl = jsonData ["primLabel"].ToString ();
			if (jsonData.ContainsKey ("primValue") && jsonData ["primValue"] != null) this.primVal = jsonData ["primValue"].ToString ();
			if (jsonData.ContainsKey ("expires") && jsonData ["expires"] != null) this.localExpTime = jsonData ["expires"].ToString ();
			if (jsonData.ContainsKey ("barcode") && jsonData ["barcode"] != null) this.barcode = jsonData ["barcode"].ToString ();
			if (jsonData.ContainsKey ("maxUses") && jsonData ["maxUses"] != null) this.maxUses = long.Parse (jsonData ["maxUses"].ToString ());
			if (jsonData.ContainsKey ("totalUses") && jsonData ["totalUses"] != null) this.totalUses = long.Parse (jsonData ["totalUses"].ToString ());
			if (jsonData.ContainsKey ("localPushDate") && jsonData ["localPushDate"] != null) this.localPushTime = jsonData ["localPushDate"].ToString ();
			if (jsonData.ContainsKey ("numUses") && jsonData ["numUses"] != null) this.numUses = long.Parse (jsonData ["numUses"].ToString ());
			if (jsonData.ContainsKey ("numOverallUses") && jsonData ["numOverallUses"] != null) this.numOverallUses = long.Parse (jsonData ["numOverallUses"].ToString ());
			if (jsonData.ContainsKey ("isExpired") && jsonData ["isExpired"] != null) this.isExpired = bool.Parse (jsonData ["isExpired"].ToString ());
			if (jsonData.ContainsKey ("twitterExpires") && jsonData ["twitterExpires"] != null) this.twitterTime = jsonData ["twitterExpires"].ToString ();
			if (jsonData.ContainsKey ("numUses") && jsonData ["numUses"] != null)
			{
				var numUses = long.Parse (jsonData ["numUses"].ToString ());
				if (maxUses > 0) maxUsesRemaining = maxUses - numUses;
			}

			if (jsonData.ContainsKey ("numOverallUses") && jsonData ["numOverallUses"] != null)
			{
				var numOverallUses = long.Parse (jsonData ["numOverallUses"].ToString ());
				if (totalUses > 0) totalUsesRemaining = totalUses - numOverallUses;
			}

			if (jsonData.ContainsKey ("cloverData") && jsonData ["cloverData"] != null)
			{
				var cloverDataString = jsonData ["cloverData"].ToString ();
				var cloverDataDictionary = JsonConvert.DeserializeObject<Dictionary<string,object>> (cloverDataString);
				if (cloverDataDictionary.ContainsKey ("Type") && cloverDataDictionary ["Type"] != null)
				{
					ICloverDiscount thisDiscount = null;
					DiscountType thisType = (DiscountType)Enum.Parse (typeof(DiscountType), cloverDataDictionary ["Type"].ToString ());
					if (thisType == DiscountType.AmountDiscount)
						thisDiscount = JsonConvert.DeserializeObject<AmountDiscount> (cloverDataString);
					else if (thisType == DiscountType.BuyXGetXDiscount)
						thisDiscount = JsonConvert.DeserializeObject<BuyXGetXDiscount> (cloverDataString);
					else if (thisType == DiscountType.PercentageDiscount)
						thisDiscount = JsonConvert.DeserializeObject<PercentageDiscount> (cloverDataString);

					if (thisDiscount != null)
						this.cloverDiscount = thisDiscount;					
				}
			}

			if (jsonData.ContainsKey ("couponVoided") && jsonData ["couponVoided"] != null) this.Cvoided = bool.Parse (jsonData ["couponVoided"].ToString ());
			else if (jsonData.ContainsKey ("voided") && jsonData ["voided"] != null) this.Cvoided = bool.Parse (jsonData ["voided"].ToString ());
			if (jsonData.ContainsKey ("offerVoided") && jsonData ["offerVoided"] != null) this.Ovoided = bool.Parse (jsonData ["offerVoided"].ToString ());

			#region old checks that may not need anymore, can parse out over time
			if (jsonData.ContainsKey ("localPushDateFormatted") && jsonData ["localPushDateFormatted"] != null) this.localPushTime = jsonData ["localPushDateFormatted"].ToString ();

			#endregion
		}
	}
}

