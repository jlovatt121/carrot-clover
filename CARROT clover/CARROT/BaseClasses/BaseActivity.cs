﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CarrotClover
{	
	public class BaseActivity : Activity
	{
		public void Transition (Type destination, int enterAnim, int exitAnim, bool shouldFinish)
		{
			Intent intent = new Intent (this, destination);

			StartActivity (intent);
			OverridePendingTransition (enterAnim, exitAnim);

			if (shouldFinish)
				this.Finish ();
		}
	}
}

