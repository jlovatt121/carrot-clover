﻿using System;
using System.ComponentModel;

using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Graphics;
using Android.Media.Audiofx;
using Android.Media;
using Android.Util;

namespace CarrotClover
{
	public class FuturaButton : Button
	{
		public FuturaButton (Context context) : base (context)
		{
			Init ();
		}

		public FuturaButton (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public FuturaButton (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public FuturaButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class FuturaBetterButton : Button
	{
		ColorFilter currentColorFilter;
		public FuturaBetterButton (Context context) : base (context)
		{
			Init ();
		}

		public FuturaBetterButton (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public FuturaBetterButton (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public FuturaBetterButton (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			base.OnTouchEvent (e);

			switch (e.Action)
			{
				case MotionEventActions.Down:
					if (this.Clickable)
					{
						this.currentColorFilter = null;
						this.Background.SetColorFilter (new Color (0x77000000), PorterDuff.Mode.SrcAtop);
						this.Invalidate ();
					}
					break;
				case MotionEventActions.Up:
				case MotionEventActions.Cancel:
					if (this.Clickable)
					{
						this.Background.ClearColorFilter ();						
						this.Invalidate ();
					}
					break;
			}
			return true;
		}
	}

	public class FuturaEditText : EditText
	{
		public FuturaEditText (Context context) : base (context)
		{
			Init ();
		}

		public FuturaEditText (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public FuturaEditText (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public FuturaEditText (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class FuturaEditTextWhiteLine : WhiteLineEditText
	{
		public FuturaEditTextWhiteLine (Context context) : base (context)
		{
			Init ();
		}

		public FuturaEditTextWhiteLine (Context context, Android.Util.IAttributeSet attr) : base (context, attr)
		{
			Init ();
		}

		public FuturaEditTextWhiteLine (Context context, Android.Util.IAttributeSet attr, int defStyle) : base (context, attr, defStyle)
		{
			Init ();
		}

		public FuturaEditTextWhiteLine (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}

	public class FuturaTextView : TextView
	{
		public FuturaTextView (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle)
		{
			Init ();
		}

		public FuturaTextView (Context context, IAttributeSet attrs) : base (context, attrs)
		{
			Init ();
		}

		public FuturaTextView (Context context) : base (context)
		{
			Init ();
		}

		public FuturaTextView (IntPtr refer, JniHandleOwnership owner) : base (refer,owner)
		{
			Init ();
		}

		private void Init ()
		{
			Typeface myTypeface = Typeface.CreateFromAsset (Context.Assets, "fonts/Futura-CondensedMedium.ttf");
			SetTypeface (myTypeface, TypefaceStyle.Normal);
		}
	}
}

