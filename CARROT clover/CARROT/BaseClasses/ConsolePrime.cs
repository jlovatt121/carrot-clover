﻿using System;

namespace CarrotClover
{
	public static class ConsolePrime
	{
		/// <summary>
		/// Used to write to the console. Works just like Console on iOS but also outputs to adb on Android
		/// </summary>
		/// <param name="str">The string to output to the Console</param>
		public static void WriteLine (string str)
		{
			#if DEBUG
			Console.WriteLine (str);
			Android.Util.Log.Info ("CARROT", str);
			#endif
		}
	}
}

