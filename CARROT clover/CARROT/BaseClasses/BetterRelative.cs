﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace CarrotClover
{
	public class BetterRelative : RelativeLayout
	{
//		float xFraction = 0;
//		float yFraction = 0;

		public float getXFraction ()
		{
			return GetX () / Width;
		}

		public void setXFraction (float xFraction)
		{
			int width = Width;
			SetX ((width > 0) ? (xFraction * width) : -9999);
		}

		public float getYFraction ()
		{
			return GetY () / Height;
		}

		public void setYFraction (float yFraction)
		{
			int height = Height;
			SetY ((height > 0) ? (yFraction * height) : -9999);
		}

		public BetterRelative (Context context) : base (context)
		{
		}
		public BetterRelative (IntPtr reference, JniHandleOwnership transfer) : base (reference, transfer)
		{
		}
		public BetterRelative (Context context, IAttributeSet attrs) : base (context,attrs)
		{
		}
		public BetterRelative (Context context, IAttributeSet attrs, int defStyle) : base (context,attrs,defStyle)
		{
		}
	}
}

