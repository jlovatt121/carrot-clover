﻿using System;

using Android.App;

namespace CarrotClover
{
	public static class User
	{
		public static string ID
		{
			get { return PlayerPrefs.GetString ("UserID"); }
			set { PlayerPrefs.SetString (value, "UserID"); }
		}

		public static string Ticket
		{
			get { return PlayerPrefs.GetString ("UserTicket"); }
			set { PlayerPrefs.SetString (value, "UserTicket"); }
		}

		public static string AppVersion
		{
			get 
			{
				var pInfo = Application.Context.PackageManager.GetPackageInfo (Application.Context.PackageName, 0);
				var version = pInfo.VersionName;
				return version;
			}
		}

		public static void ClearUserData ()
		{
			ID = "";
			Ticket = "";
		}
	}
}

