﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Net;
using System.IO;

using Android.App;
using Android.Support.V4.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;

using Com.Clover.Sdk.Util;
using Com.Clover.Sdk.V1;
using Com.Clover.Sdk.V3.Order;
using Com.Clover.Sdk.V3.Inventory;

using ZXing;
using ZXing.Mobile;

using AndroidHUD;

using Newtonsoft.Json;

namespace CarrotClover
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar.Fullscreen", ConfigurationChanges=ConfigChanges.Orientation|ConfigChanges.KeyboardHidden)]
	[IntentFilter (new [] {"clover.intent.action.MODIFY_ORDER"}, Categories = new [] {Intent.CategoryDefault})]
	public class ScanBarcodeActivity : global::Android.Support.V4.App.FragmentActivity
	{
		string orderID;

		View scanOverlay;

		string scannedBarcode;
		ICloverDiscount thisDiscount;
		LineItem discountedItem;

		BetterTextView discountNameText;
		RobotoTextView discountDescriptionText;

		Button redeemButton;
		BetterImageButton cancelButton;

		RelativeLayout scannedLayout;

		ZXingScannerFragment scanFragment;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ScanBarcodeActivityLayout);
			orderID = Intent.GetStringExtra (Intents.ExtraOrderId);

			// Set screen items to usable variables
			discountNameText = FindViewById<BetterTextView> (Resource.Id.scanner_discountName);
			discountDescriptionText = FindViewById<RobotoTextView> (Resource.Id.scanner_discountDescription);

			redeemButton = FindViewById<Button> (Resource.Id.scanner_redeemButton);
			cancelButton = FindViewById<BetterImageButton> (Resource.Id.scanner_backButton);

			scannedLayout = FindViewById<RelativeLayout> (Resource.Id.scanner_scannedLayout);

			// Set defaults
			scannedLayout.Visibility = ViewStates.Gone;
			scanOverlay = LayoutInflater.Inflate (Resource.Layout.barcodeLayoutScanning,null,false);
			redeemButton.Clickable = false;

			// Set handlers
			redeemButton.Click += RedeemButton_Click;
			cancelButton.Click += CancelButton_Click;
		}

		protected override void OnStart ()
		{
			base.OnStart ();
			StartScanner ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
		}

		protected override void OnStop ()
		{
			AndHUD.Shared.Dismiss (this);
			base.OnStop ();
		}

		void CancelButton_Click (object sender, EventArgs e)
		{
			this.Finish ();
		}

		void RedeemButton_Click (object sender, EventArgs e)
		{
			AndHUD.Shared.Show (this, "Redeeming...", timeout: null);
			new Thread (new ThreadStart (delegate
			{
				RedeemCouponOnServer ();
			})).Start ();
		}

		void StartScanner ()
		{
			RunOnUiThread (() =>
			{
				scanFragment = new ZXingScannerFragment ();

				scanFragment.UseCustomOverlayView = true;
				scanFragment.CustomOverlayView = scanOverlay;

				SupportFragmentManager.BeginTransaction ()
					.Add (Resource.Id.scanCoupFrame, scanFragment)
					.Commit ();

				var opts = new MobileBarcodeScanningOptions {
					PossibleFormats = new List<ZXing.BarcodeFormat> {
						ZXing.BarcodeFormat.QR_CODE
					},
					UseFrontCameraIfAvailable = true,
					CameraResolutionSelector = availableResolutions => {
						foreach (var ar in availableResolutions) {
							Console.WriteLine ("Resolution: " + ar.Width + "x" + ar.Height);
						}
						return null;
					}
				};

				scanFragment.StartScanning (opts,(result) =>
				{
					if (result == null || string.IsNullOrWhiteSpace (result.Text))
					{
						RunOnUiThread (() => Toast.MakeText (this, "Had trouble scanning, try again", ToastLength.Short).Show());
						return;
					}
					RunOnUiThread (() =>
					{
						scanOverlay.FindViewById<ImageView> (Resource.Id.scannerBox).SetImageResource (Resource.Drawable.green_border_square);
						scanOverlay.FindViewById<View> (Resource.Id.scannerHorizontalLine).Visibility = ViewStates.Gone;
						scanFragment.StopScanning();
					});
					HandleScanResult (result.Text);
				});
			});
		}

		void HandleScanResult (string barcode)
		{
			
			var stringArray = barcode.Split (new char[] { ';' });

			if (stringArray.Length == 4)
			{
				AndHUD.Shared.Show (this, "Validating...", -1, MaskType.Black, null);
				// It's a good barcode
				new Thread (new ThreadStart (delegate
				{
					CheckValidCoupon (barcode);
				})).Start ();
			}
			else
			{
				// It's bunk
				Alerts.PresentAlert (this, "Error", "Invalid barcode scanner, please try again");
			}
		}

		void CheckValidCoupon (string barcode)
		{
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/redeemcoupon.aspx", "POST", "barcode=" + barcode + "&isCloverCheck=true", (thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == System.Net.HttpStatusCode.Accepted)
				{
					scannedBarcode = barcode;

					var coupDictionary = JsonConvert.DeserializeObject<Dictionary<string,object>> (answer);
					if (coupDictionary.ContainsKey ("Type") && coupDictionary ["Type"] != null)
					{
						DiscountType thisType = (DiscountType)Enum.Parse (typeof(DiscountType),coupDictionary["Type"].ToString());
						if (thisType == DiscountType.AmountDiscount)
						{
							thisDiscount = JsonConvert.DeserializeObject<AmountDiscount> (answer);
							if (((AmountDiscount)thisDiscount).lineItemID == "-1")
								UpdateUINoItems ();
							else ValidateDiscountAgainstInventory ();
						}
//						else if (thisType == DiscountType.BuyXGetXDiscount)
//						{
//							thisDiscount = JsonConvert.DeserializeObject<BuyXGetXDiscount> (answer);
//						}
						else if (thisType == DiscountType.PercentageDiscount)
						{
							thisDiscount = JsonConvert.DeserializeObject<PercentageDiscount> (answer);
							if (((PercentageDiscount)thisDiscount).lineItemID == "-1")
								UpdateUINoItems ();
							else ValidateDiscountAgainstInventory ();
						}
						else
						{
							AndHUD.Shared.Dismiss (this);
							// Weird JSON, treat it like a 200
							RunOnUiThread (() => Alerts.PresentAlert (this, "Malformed data", "The Discount data was malformed. Try again. If the problem persists, contact a Manager."));
						}
					}
					else
					{
						AndHUD.Shared.Dismiss (this);
						// Weird JSON, treat it as a 200
						RunOnUiThread (() => Alerts.PresentAlert (this, "Malformed data", "The Discount data was malformed. Try again. If the problem persists, contact a Manager."));
					}
				}
				else
				{
					AndHUD.Shared.Dismiss (this);
					// ERROR....ERROR...ERROR
					string error = answer.Substring (0,3);
					switch (error)
					{
						case "E1:":
						case "E2:":
						case "E3:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Coupon Error", "Coupon is expired or voided or max has been reached."));
							break;
						case "E4:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Subscription Error", "Store is currently not active."));
							break;
						case "E5:":
						case "E7:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Coupon Error", "Barcode or coupon is invalid."));
							break;
						case "E6:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Permission Error", "This coupon is for another store."));
							break;
						case "E8:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Reward Error", "User does not have enough reward points to redeem this coupon."));
							break;
						case "E9:":
							RunOnUiThread (() => Alerts.PresentAlert (this, "No Discount", "A Clover discount has not been linked to this CARROT deal. Contact a Manager"));
							break;
						default:
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Unknown Error", "Unknown Error."));
							break;
					}
				}
			});
		}

		void RedeemCouponOnServer ()
		{
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/redeemcoupon.aspx", "POST", "barcode=" + scannedBarcode, (thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == HttpStatusCode.Accepted)
				{
					OrderSaver.LinkBarcodeToOrder (orderID,scannedBarcode);

					OrderConnector orderConnector = new OrderConnector(this, CloverAccount.GetAccount(this),null);
					orderConnector.Connect();

					Com.Clover.Sdk.V3.Order.Discount discount = new Com.Clover.Sdk.V3.Order.Discount();
					if (thisDiscount.GetType () == typeof (AmountDiscount))
					{
						discount = ((AmountDiscount)thisDiscount).GetDiscount();
					}
					else if (thisDiscount.GetType () == typeof(PercentageDiscount))
					{
						discount = ((PercentageDiscount)thisDiscount).GetDiscount();
					}

					if (discountedItem == null)
						orderConnector.AddDiscount (orderID, discount);
					else orderConnector.AddLineItemDiscount (orderID, discountedItem.Id, discount);

					orderConnector.Disconnect();
					AndHUD.Shared.Dismiss(this);
					RunOnUiThread (() => this.Finish ());
				}
				else
				{
					AndHUD.Shared.Dismiss (this);
					// ERROR....ERROR...ERROR
					string error = answer.Substring (0,3);
					switch (error)
					{
						case "E1:":
						case "E2:":
						case "E3:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Coupon Error", "Coupon is expired or voided or max has been reached."));
							break;
						case "E4:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Subscription Error", "Store is currently not active."));
							break;
						case "E5:":
						case "E7:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Coupon Error", "Barcode or coupon is invalid."));
							break;
						case "E6:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Permission Error", "This coupon is for another store."));
							break;
						case "E8:":
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Reward Error", "User does not have enough reward points to redeem this coupon."));
							break;
						case "E9:":
							RunOnUiThread (() => Alerts.PresentAlert (this, "No Discount", "A Clover discount has not been linked to this CARROT deal. Contact a Manager"));
							break;
						default:
							RunOnUiThread(()=> Alerts.PresentAlert (this,"Unknown Error", "Unknown Error."));
							break;
					}
				}
			});
		}

		void ValidateDiscountAgainstInventory ()
		{
			string thisDiscountItemID = "";
			if (thisDiscount.GetType () == typeof(PercentageDiscount))
				thisDiscountItemID = ((PercentageDiscount)thisDiscount).lineItemID;
			else if (thisDiscount.GetType () == typeof(AmountDiscount))
				thisDiscountItemID = ((AmountDiscount)thisDiscount).lineItemID;

			if (string.IsNullOrWhiteSpace (thisDiscountItemID))
			{
				AndHUD.Shared.Dismiss (this);
				Alerts.PresentAlert (this, "Error", "Received bad data from the server, please try again or contact a manager.");
				return;
			}

			InventoryConnector inventoryConnector = new InventoryConnector (this, CloverAccount.GetAccount (this), null);
			OrderConnector orderConnector = new OrderConnector (this, CloverAccount.GetAccount (this), null);

			inventoryConnector.Connect ();
			orderConnector.Connect ();

			IList<Item> inventoryItems = inventoryConnector.Items;
			IList<LineItem> orderItems = orderConnector.GetOrder (orderID).LineItems;
			List<LineItem> itemsThatMatch = new List<LineItem> ();
			discountedItem = null;

			bool passedInventoryCheck = false;
			bool passedOrderCheck = false;

			foreach (Item item in inventoryItems)
			{
				if (item.Id == thisDiscountItemID)
				{
					passedInventoryCheck = true;
					break;
				}
			}

			if (!passedInventoryCheck)
			{
				AndHUD.Shared.Dismiss (this);
				Alerts.PresentAlert (this, "Inventory Error", "Could not find the associated item in your inventory. Please contact a manager.");
				return;
			}

			foreach (LineItem lineItem in orderItems)
			{
				if (lineItem.Item.Id == thisDiscountItemID)
				{
					itemsThatMatch.Add (lineItem);
					passedOrderCheck = true;
				}
			}

			if (!passedOrderCheck)
			{
				AndHUD.Shared.Dismiss (this);
				Alerts.PresentAlert (this, "Order Error", "Could not find the asseciated item in the current Order. Please add the correct item to the order and try again.");
				return;
			}

			if (itemsThatMatch.Count > 1)
			{
				RunOnUiThread (() =>
				{
					AndHUD.Shared.Dismiss (this);
					AlertDialog alertBuilder = new AlertDialog.Builder (this).Create();
					ListView listView = new ListView (this);
					alertBuilder.SetView (listView);
					alertBuilder.SetTitle ("Select item to apply discount to");
					LineItemPickerAdapter adapter = new LineItemPickerAdapter (this, Resource.Layout.line_item_picker_row, itemsThatMatch);
					listView.Adapter = adapter;

					listView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) => 
					{
						discountedItem = itemsThatMatch[e.Position];
						UpdateUIWithItems (discountedItem);
						alertBuilder.Cancel();
					};
					alertBuilder.SetCancelable (false);
					alertBuilder.SetCanceledOnTouchOutside (false);
					alertBuilder.Show ();
				});
			}
			else
			{
				discountedItem = itemsThatMatch[0];
				UpdateUIWithItems (discountedItem);
			}
		}

		void UpdateUINoItems ()
		{					
			AndHUD.Shared.Dismiss (this);
			RunOnUiThread (() =>
			{
				scannedLayout.Visibility = ViewStates.Visible;
				scannedLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));

				if (thisDiscount.GetType () == typeof(AmountDiscount))
				{
					discountNameText.Text = ((AmountDiscount)thisDiscount).discountName;
					Decimal dividedCents = (Decimal)(((AmountDiscount)thisDiscount).amountOff / 100);
					discountDescriptionText.Text = "Redeeming this will apply a discount of " + string.Format ("{0:C}",dividedCents) + " to the whole order.";
				}
				else if (thisDiscount.GetType () == typeof(PercentageDiscount))
				{
					discountNameText.Text = ((PercentageDiscount)thisDiscount).discountName;
					discountDescriptionText.Text = "Redeeming this will take " + ((PercentageDiscount)thisDiscount).percentageOff + "% off your whole order"; 
				}

				redeemButton.Clickable = true;
			});
		}

		void UpdateUIWithItems (LineItem discountedItem)
		{
			AndHUD.Shared.Dismiss (this);
			RunOnUiThread (() =>
			{
				scannedLayout.Visibility = ViewStates.Visible;
				scannedLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));

				if (thisDiscount.GetType () == typeof(AmountDiscount))
				{
					discountNameText.Text = ((AmountDiscount)thisDiscount).discountName;
					Decimal dividedCents = (Decimal)(((AmountDiscount)thisDiscount).amountOff / 100);
					discountDescriptionText.Text = "Redeeming this will apply a discount of " + string.Format ("{0:C}",dividedCents) + " to " + discountedItem.Name;
				}
				else if (thisDiscount.GetType () == typeof(PercentageDiscount))
				{
					discountNameText.Text = ((PercentageDiscount)thisDiscount).discountName;
					discountDescriptionText.Text = "Redeeming this will take " + ((PercentageDiscount)thisDiscount).percentageOff + "% off " + discountedItem.Name + "'s cost"; 
				}

				redeemButton.Clickable = true;
			});
		}

//		void ScanButton_Click (object sender, EventArgs e)
//		{
//			new Thread (new ThreadStart (delegate
//			{
//				OrderConnector orderConnector = new OrderConnector (this, CloverAccount.GetAccount(this), null);
//				orderConnector.Connect ();
//				InventoryConnector inventoryConnector = new InventoryConnector (this, CloverAccount.GetAccount(this), null);
//				inventoryConnector.Connect ();
//
//				var item2ID = inventoryConnector.Items[1].Id;
//					
//				Com.Clover.Sdk.V3.Order.Discount discount = new Com.Clover.Sdk.V3.Order.Discount();
//				discount.SetAmount (Java.Lang.Long.ValueOf(-10));
//				discount.SetName ("Carrot 10 off");
//
//				orderConnector.AddDiscount (orderID, discount);
//
//				orderConnector.Disconnect ();
//				inventoryConnector.Disconnect ();
//				this.Finish ();
//
//				var orders = orderConnector.GetOrder(orderID);
//				var items = orders.LineItems;
//
//				bool foundItem = false;
//				bool allItemsHaveDiscount = true;
//				foreach (LineItem lineItem in items)
//				{
//					if (lineItem.Item.Id == item2ID)
//					{
//						foundItem = true;
//						if (lineItem.HasDiscounts == false)
//						{
//							allItemsHaveDiscount = false;
//							orderConnector.AddLineItemDiscount(orderID, lineItem.Id, discount);
//
//							break;
//						}
//					}
//				}
//
//				if (!foundItem)
//				{
//					RunOnUiThread (() =>
//						Alerts.PresentAlert (this, "Item Not Found","We could not find the item this deal is for in the current order. Try again when that item has been added to the order", () =>
//						{
//							orderConnector.Disconnect ();
//							inventoryConnector.Disconnect ();
//
//							this.Finish ();
//						}));
//				}
//				else if (allItemsHaveDiscount)
//				{
//					RunOnUiThread (() =>
//	               		Alerts.PresentAlert (this,"Discounts Exceeded","All items in this order have the discount applied to them.", () =>
//						{
//							orderConnector.Disconnect ();
//							inventoryConnector.Disconnect ();
//
//							this.Finish ();
//						}));
//				}
//				else
//				{
//					orderConnector.Disconnect ();
//					inventoryConnector.Disconnect ();
//
//					this.Finish ();
//				}
//			})).Start ();
//		}
	}


}

