﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Threading;

using Parse;

namespace CarrotClover
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar.Fullscreen")]			
	public class LoginActivity : Activity
	{
		EditText email;
		EditText username;
		EditText password;

		Button register;
		Button loginout;

		ParseUser currentUser;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.LoginLayout);

			// Create your application here
			email = FindViewById<EditText> (Resource.Id.email);
			username = FindViewById<EditText> (Resource.Id.username);
			password = FindViewById<EditText> (Resource.Id.password);

			register = FindViewById<Button> (Resource.Id.register);
			loginout = FindViewById<Button> (Resource.Id.login);

			currentUser = ParseUser.CurrentUser;
			register.Click += Register_Click;

			if (currentUser == null)
			{
				email.Text = "";
				username.Text = "";
				password.Text = "";
				email.Enabled = true;
				email.Clickable = true;
				username.Enabled = true;
				username.Clickable = true;
				password.Enabled = true;
				password.Clickable = true;

				register.Visibility = ViewStates.Visible;
				loginout.Text = "Log In";
				loginout.Click += Login;
			}
			else
			{
				email.Text = currentUser.Email;
				username.Text = currentUser.Username;
				password.Text = "********";
				email.Enabled = false;
				email.Clickable = false;
				username.Enabled = false;
				username.Clickable = false;
				password.Enabled = false;
				password.Clickable = false;

				register.Visibility = ViewStates.Gone;
				loginout.Text = "Log Out";
				loginout.Click += Logout;
			}
		}

		void Login (object sender, EventArgs e)
		{			
			new Thread (new ThreadStart (async delegate {
				try
				{			
					AndroidHUD.AndHUD.Shared.Show (this,timeout:null);
					await ParseUser.LogInAsync (username.Text, password.Text);
					AndroidHUD.AndHUD.Shared.Dismiss (this);
					RunOnUiThread (() => 
					{						
						currentUser = ParseUser.CurrentUser;
						email.Text = currentUser.Email;
						username.Text = currentUser.Username;
						password.Text = "********";
						email.Enabled = false;
						email.Clickable = false;
						username.Enabled = false;
						username.Clickable = false;
						password.Enabled = false;
						password.Clickable = false;

						loginout.Text = "Log out";
						register.Visibility = ViewStates.Gone;
						loginout.Click -= Login;
						loginout.Click += Logout;
					});						
				}
				catch (ParseException ex)
				{
					AndroidHUD.AndHUD.Shared.Dismiss (this);
					Alerts.PresentAlert (this, "Mo Fugin Error", ex.Code + " " + ex.Message);
				}
			})).Start();
		}

		void Logout (object sender, EventArgs e)
		{
			try
			{
				AndroidHUD.AndHUD.Shared.Show (this,timeout:null);
				ParseUser.LogOut ();
				AndroidHUD.AndHUD.Shared.Dismiss (this);

				email.Text = "";
				username.Text = "";
				password.Text = "";
				email.Enabled = true;
				email.Clickable = true;
				username.Enabled = true;
				username.Clickable = true;
				password.Enabled = true;
				password.Clickable = true;

				loginout.Text = "Login";
				register.Visibility = ViewStates.Visible;
				loginout.Click -= Logout;
				loginout.Click += Login;
			}
			catch (ParseException ex)
			{
				AndroidHUD.AndHUD.Shared.Dismiss (this);
				Alerts.PresentAlert (this, "Mo Fugin Error", ex.Code + " " + ex.Message);
			}
		}

		void Register_Click (object sender, EventArgs e)
		{
			var user = new ParseUser () {
				Email = email.Text,
				Username = username.Text,
				Password = password.Text
			};
			new Thread (new ThreadStart (async delegate {
				try
				{	
					AndroidHUD.AndHUD.Shared.Show (this,timeout:null);
					await user.SignUpAsync ();
					AndroidHUD.AndHUD.Shared.Dismiss (this);
					RunOnUiThread (() => 
					{						
						currentUser = ParseUser.CurrentUser;
						email.Text = currentUser.Email;
						username.Text = currentUser.Username;
						password.Text = "********";
						email.Enabled = false;
						email.Clickable = false;
						username.Enabled = false;
						username.Clickable = false;
						password.Enabled = false;
						password.Clickable = false;

						loginout.Text = "Log out";
						register.Visibility = ViewStates.Gone;
						loginout.Click -= Login;
						loginout.Click += Logout;
					});					
				}
				catch (ParseException ex)
				{
					AndroidHUD.AndHUD.Shared.Dismiss (this);
					Alerts.PresentAlert (this, "Mo Fugin Error", ex.Code + " " + ex.Message);
				}
			})).Start();
		}
	}
}

