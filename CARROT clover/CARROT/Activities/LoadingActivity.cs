﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CarrotClover
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar.Fullscreen", MainLauncher = true)]			
	public class LoadingActivity : BaseActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.LoadingActivityLayout);

			Load ();
		}

		protected override void OnStart ()
		{
			base.OnStart ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}

		private void Load ()
		{
			Transition (typeof(LoginActivity), Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut, true);
//			if (!string.IsNullOrWhiteSpace (User.ID) && !string.IsNullOrWhiteSpace (User.Ticket))
//			{
//				ConsolePrime.WriteLine ("Already registered");
//				new Thread (new ThreadStart (delegate
//				{
//					Thread.Sleep (2000);
//					RunOnUiThread (() =>
//					{
//						Transition (typeof(MainActivity), Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut, true);
//					});
//				})).Start ();
//			} 
//			else
//			{
//				ConsolePrime.WriteLine ("Needs to register");
//				User.ClearUserData ();
//				new Thread (new ThreadStart (delegate
//				{
//					RegisterUser ();
//				})).Start ();
//			}
		}

//		private void RegisterUser()
//		{
//			WebRequest request = WebRequest.Create ("https://carrot.azurewebsites.net/adminregistration.aspx");
//			request.Method = "GET";
//			request.ContentType = "application/x-www-form-urlencoded";
//			request.Headers.Add ("cage", "carrotApp");
//			request.Headers.Add ("connery", "FfktyvyHszvnaWPEhOP3w-AyphVBlmQ1");
//			request.Headers.Add ("appPlatform", "Clover");
//			request.Headers.Add ("versionNum", User.AppVersion);
//
//			try
//			{
//				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse ())
//				{
//					using (StreamReader reader = new StreamReader (response.GetResponseStream ()))
//					{
//						string answer = reader.ReadToEnd ();
//						if (response.StatusCode == HttpStatusCode.Accepted)
//						{
//							ConsolePrime.WriteLine ("Registered Successfully!");
//							if (!string.IsNullOrWhiteSpace (answer)) User.ID = answer;
//
//							if (response.Headers.AllKeys.Contains ("userTicket"))
//								User.Ticket = response.Headers ["userTicket"].ToString ();
//								
//							Thread.Sleep (2000);
//							RunOnUiThread (() => Transition (typeof(MainActivity), Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut, true));
//						}
//						else
//						{
//							ConsolePrime.WriteLine ("Registration Failed!");
//						}
//					}
//				}
//			}
//			catch (Exception ex)
//			{
//				ConsolePrime.WriteLine ("Registration catch error!");
//			}
//		}
	}
}

