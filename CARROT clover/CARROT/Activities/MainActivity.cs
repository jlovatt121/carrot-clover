﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Web;

using Android.App;
using Android.Support.V4.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Webkit;

using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

using AndroidHUD;

using ZXing.Mobile;

using Java.Util;
using Java.Util.Regex;

using Com.Clover.Sdk.Util;
using Com.Clover.Sdk.V1;
using Com.Clover.Sdk.V3.Order;
using Com.Clover.Sdk.V3.Employees;

namespace CarrotClover
{
	public enum MainState
	{
		Intro,
		Contact,
		Scanning,
		HasFrans
	}



	[BroadcastReceiver (Enabled = true)]
	[IntentFilter (new [] {"com.clover.intent.action.PAYMENT_PROCESSED"})]
	public class OrderCompleteListener : BroadcastReceiver
	{
		public override void OnReceive (Context context, Intent intent)
		{
			string action = intent.Action;
			if (action == "com.clover.intent.action.PAYMENT_PROCESSED")
			{
				string orderID = intent.GetStringExtra ("com.clover.intent.extra.ORDER_ID");
				long amount = intent.GetLongExtra("com.clover.intent.extra.AMOUNT",0);
				List<string> barcodes = OrderSaver.BarcodesForOrderID (orderID);

				if (barcodes.Count >= 0)
				{
					// There was an order saved
					new Thread (new ThreadStart (delegate
					{
						//TODO send order amount along with discounts to Jon
						string commaSeperatedBarcodes = string.Join (",",barcodes);
						string postString = "totalAmount=" + HttpUtility.UrlEncode(amount.ToString()) + "&posSystem=Clover" + "&barcodes=" + HttpUtility.UrlEncode(commaSeperatedBarcodes);
						WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/logpostransaction.aspx","POST",postString,thisAnswer =>
						{
							var answer = thisAnswer.answer;
							var statusCode = thisAnswer.statusCode;

							if (statusCode == HttpStatusCode.Accepted)
								ConsolePrime.WriteLine ("Payment Sent: " + answer);
							else ConsolePrime.WriteLine ("Payment failed To Send: " + answer);

							OrderSaver.RemoveOrderLink (orderID);
						});
					})).Start ();
				}
			}
		}
	}

	[Activity (Theme = "@style/_AppTheme", ConfigurationChanges=ConfigChanges.Orientation|ConfigChanges.KeyboardHidden,WindowSoftInputMode = SoftInput.StateHidden|SoftInput.AdjustPan)]
	public class MainActivity : global::Android.Support.V4.App.FragmentActivity
	{
		public static bool needsToRefresh = false;

		public MainState state = MainState.Intro;

		TextView machineIDText;
		TextView headerTitleText;
		Button addStoreButton;

		FranchiseInfo franForNewCoupon;
		Coupon couponForNewCloverDiscount;

		RelativeLayout introLayout;
		RelativeLayout contactInfoLayout;
		RelativeLayout hasFransLayout;
		LinearLayout scanningLayout;
		RelativeLayout dealCreationLayout;

		#region Intro Layout Variables
		Button registerButton;
		Button scanQRButton;
		#endregion

		#region Contact Info Variables
		Button contactCancelButton;
		Button submitButton;
		WebView contactWebview;
		EditText storeNameText;
		EditText yourNameText;
		EditText emailText;
		EditText phoneNumberText;
		#endregion

		#region Scanning Variables
		ZXingScannerFragment scanFragment;
		RelativeLayout scannerFrame;
		RelativeLayout scannerInfoLayout;
		BetterButton nextButton;
		BetterButton cancelScanningButton;
		View scanOverlay;
		TextView scannerText;
		#endregion

		#region Has Frans Variables
		bool refreshingHasFrans = false;
		List<FransWithCoups> currentFrans = new List<FransWithCoups> ();
		ListView hasFransList;
		HasFransFranAdapter adapter;
		SwipeRefreshLayout swipeRefreshLayout;
		#endregion

		#region Info variables
		RelativeLayout infoLayout;
		ImageButton infoButton;
		Button infoCloseButton;
		#endregion

		#region DealVariables
		ImageView couponBlocker;
		bool creatingFullDeal = false;
		FuturaTextView storeTextView;

		RobotoEditText offerEditText;
		string offerText;
		RobotoEditText itemEditText;
		string itemText;

		CompoundButton prioritizedCompoundButton;
		bool prioritized;

		RobotoButton selectExpirationDateButton;
		RobotoAutoResizeTextView expirationText;
		DatePickerDialog dateDialog;
		TimePickerDialogIntervals timeDialog;
		DateTime expirationDate;

		List<string> points = new List<string> () {"0", "5,000", "10,000", "20,000", "50,000", "100,000", "500,000"};
		ArrayAdapter<string> rewardPointsAdapter;
		RobotoButton selectRewardPointsButton;
		long dealRewardPoints;

		RobotoEditText totalUsesEditText;
		long totalUses;
		RobotoEditText maxUsesEditText;
		long maxUses;

		RobotoEditText cloverDiscountName;
		RobotoRadioButton wholeOrderRadioButton;
		RobotoRadioButton singleItemRadioButton;
		RobotoRadioButton percentageRadioButton;
		RobotoRadioButton amountRadioButton;

		RobotoButton selectItemButton;
		string dealItemID;
		RobotoEditText percentageEditText;
		RobotoEditText amountEditText;
		string currentAmount = "";

		RobotoBetterButton dealSubmitButton;
		RobotoBetterButton dealCancelButton;
		#endregion

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// Create your application here
//			SetContentView (Resource.Layout.MainActivityLayout);
			SetContentView (Resource.Layout.MainScreenNewLayout);
			// Set screen resources to usable variables
			machineIDText = FindViewById<TextView> (Resource.Id.Main_userIDText);
			headerTitleText = FindViewById<TextView> (Resource.Id.Main_header_pageTitle);
			addStoreButton = FindViewById<Button> (Resource.Id.addStoreButton);

			introLayout = FindViewById<RelativeLayout> (Resource.Id.Main_introLayout);
			contactInfoLayout = FindViewById<RelativeLayout> (Resource.Id.Main_contactInfoLayout);
			hasFransLayout = FindViewById<RelativeLayout> (Resource.Id.Main_hasFransLayout);
			scanningLayout = FindViewById<LinearLayout> (Resource.Id.Main_scanScreen);
			dealCreationLayout = FindViewById<RelativeLayout> (Resource.Id.Main_dealCreationScreen);

			registerButton = FindViewById<Button> (Resource.Id.Main_intro_registerButton);
			scanQRButton = FindViewById<Button> (Resource.Id.Main_intro_scanButton);

			contactCancelButton = FindViewById<Button> (Resource.Id.Main_contact_cancelButton2);
			submitButton = FindViewById<Button> (Resource.Id.Main_contact_submitButton);
			contactWebview = FindViewById<WebView> (Resource.Id.Main_contact_webView);
			storeNameText = FindViewById<EditText> (Resource.Id.Main_contact_storeNameText);
			yourNameText = FindViewById<EditText> (Resource.Id.Main_contact_yourNameText);
			emailText = FindViewById<EditText> (Resource.Id.Main_contact_emailText);
			phoneNumberText = FindViewById<EditText> (Resource.Id.Main_contact_phoneText);

			scannerFrame = FindViewById<RelativeLayout> (Resource.Id.Main_scan_scannerFrame);
			scannerInfoLayout = FindViewById<RelativeLayout> (Resource.Id.Main_scan_info);
			nextButton = FindViewById<BetterButton> (Resource.Id.Main_scan_nextButton);
			cancelScanningButton = FindViewById<BetterButton> (Resource.Id.Main_scan_cancelButton);
			scannerText = FindViewById<TextView> (Resource.Id.Main_scan_scannerText);

			infoLayout = FindViewById<RelativeLayout> (Resource.Id.Main_infoScreen);
			infoButton = FindViewById<ImageButton> (Resource.Id.Main_infoButton);
			infoCloseButton = FindViewById<Button> (Resource.Id.Main_info_closeButton);

			couponBlocker = FindViewById<ImageView> (Resource.Id.Main_deal_couponBlocker);
			storeTextView = FindViewById<FuturaTextView> (Resource.Id.Main_deal_storeNameText);
			offerEditText = FindViewById<RobotoEditText> (Resource.Id.Main_deal_offerText);
			itemEditText = FindViewById<RobotoEditText> (Resource.Id.Main_deal_itemText);
			prioritizedCompoundButton = FindViewById<CompoundButton> (Resource.Id.Main_deal_prioritizedToggle);
			selectExpirationDateButton = FindViewById<RobotoButton> (Resource.Id.Main_deal_dateButton);
			expirationText = FindViewById<RobotoAutoResizeTextView> (Resource.Id.Main_deal_dateText);
			selectRewardPointsButton = FindViewById<RobotoButton> (Resource.Id.Main_deal_rewardPointsButton);
			totalUsesEditText = FindViewById<RobotoEditText> (Resource.Id.Main_deal_totalUsesInput);
			maxUsesEditText = FindViewById<RobotoEditText> (Resource.Id.Main_deal_maxUsesInput);
			cloverDiscountName = FindViewById<RobotoEditText> (Resource.Id.Main_deal_cloverName);
			wholeOrderRadioButton = FindViewById<RobotoRadioButton> (Resource.Id.Main_deal_wholeOrderRadio);
			singleItemRadioButton = FindViewById<RobotoRadioButton> (Resource.Id.Main_deal_singleItemRadio);
			percentageRadioButton = FindViewById<RobotoRadioButton> (Resource.Id.Main_deal_percentageRadio);
			amountRadioButton = FindViewById<RobotoRadioButton> (Resource.Id.Main_deal_amountRadio);
			selectItemButton = FindViewById<RobotoButton> (Resource.Id.Main_deal_itemSelectButton);
			percentageEditText = FindViewById<RobotoEditText> (Resource.Id.Main_deal_percentageInput);
			amountEditText = FindViewById<RobotoEditText> (Resource.Id.Main_deal_amountInput);
			dealSubmitButton = FindViewById<RobotoBetterButton> (Resource.Id.Main_deal_submitButton);
			dealCancelButton = FindViewById<RobotoBetterButton> (Resource.Id.Main_deal_cancelButton);

			hasFransList = FindViewById<ListView> (Resource.Id.Main_HF_list);
			swipeRefreshLayout = FindViewById<SwipeRefreshLayout> (Resource.Id.refresher);
			adapter = new HasFransFranAdapter (this, Resource.Layout.has_fran_list_item, currentFrans);
			hasFransList.Adapter = adapter;

			// Set defaults
			swipeRefreshLayout.SetColorSchemeColors (Resource.Color.xam_dark_blue,
				Resource.Color.xam_purple,
				Resource.Color.xam_gray,
				Resource.Color.xam_green);			

			machineIDText.Text = "ID: " + User.ID;

			scanOverlay = LayoutInflater.Inflate (Resource.Layout.barcodeLayoutScanning,null,false);

			contactWebview.SetWebViewClient (new WebViewClient ());
			contactWebview.SetWebChromeClient (new WebChromeClient ());
			contactWebview.Settings.JavaScriptEnabled = true;

			rewardPointsAdapter = new ArrayAdapter<string> (this, Resource.Layout.rewards_points_adapter_row, points);
			rewardPointsAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
//			FindViewById<RelativeLayout> (Resource.Id.Main_body).RemoveView (infoButton);
//			hasFransList.AddHeaderView (infoButton);

			// Set handlers
			registerButton.Click += RegisterButton_Click;
			scanQRButton.Click += ScanQRButton_Click1;

			contactCancelButton.Click += ContactCancelButton_Click;

			nextButton.Click += NextButton_Click;
			cancelScanningButton.Click += CancelScanningButton_Click;

			addStoreButton.Click += AddStoreButton_Click;
			swipeRefreshLayout.Refresh += SwipeRefreshLayout_Refresh;

			infoButton.Click += InfoButton_Click;
			infoCloseButton.Click += InfoCloseButton_Click;

			adapter.NewCoupSelected += Adapter_NewCoupSelected;

			prioritizedCompoundButton.CheckedChange += PrioritizedCompoundButton_CheckedChange;
			selectExpirationDateButton.Click += SelectExpirationDateButton_Click;
			selectRewardPointsButton.Click += SelectRewardPointsButton_Click;
			selectItemButton.Click += SelectItemButton_Click;
			wholeOrderRadioButton.CheckedChange += WholeOrderRadioButton_CheckedChange;
			singleItemRadioButton.CheckedChange += SingleItemRadioButton_CheckedChange;
			percentageRadioButton.CheckedChange += PercentageRadioButton_CheckedChange;
			amountRadioButton.CheckedChange += AmountRadioButton_CheckedChange;
			amountEditText.TextChanged += AmountEditText_TextChanged;
			dealCancelButton.Click += DealCancelButton_Click;
			dealSubmitButton.Click += DealSubmitButton_Click;

			new System.Threading.Thread (new ThreadStart (delegate
			{
				CheckForFranchises();
			})).Start ();


		}

		protected override void OnStart ()
		{
			base.OnStart ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}

		protected override void OnDestroy ()
		{
			AndHUD.Shared.Dismiss (this);
			base.OnDestroy ();
		}

		public override void OnBackPressed ()
		{
			if (state == MainState.Contact)
				ContactCancelButton_Click (this, null);
			else if (state == MainState.Scanning)
				CancelScanningButton_Click (this, null);
			else base.OnBackPressed ();
		}

		#region deal functions
		void ClearDealCreation ()
		{
			couponBlocker.Visibility = ViewStates.Gone;
			storeTextView.Text = "";
			offerEditText.Text = "";
			offerText = "";
			itemEditText.Text = "";
			itemText = "";
			prioritizedCompoundButton.Checked = false;
			prioritized = false;
			expirationDate = DateTime.UtcNow.AddDays (1);
			expirationDate = expirationDate.ToLocalTime ();
			expirationDate = new DateTime (expirationDate.Year, expirationDate.Month, expirationDate.Day, expirationDate.Hour, 0, 0);
			expirationText.Text = expirationDate.ToString ("ddd, MMM d, yyyy '@' hh:mm tt");
			selectRewardPointsButton.Text = "0";
			dealRewardPoints = 0;
			totalUsesEditText.Text = "";
			totalUses = -1;
			maxUsesEditText.Text = "";
			maxUses = -1;
			cloverDiscountName.Text = "";
			wholeOrderRadioButton.Checked = true;
			percentageRadioButton.Checked = true;
			selectItemButton.Text = "";
			selectItemButton.Clickable = false;
			selectItemButton.Enabled = false;
			dealItemID = "";
			percentageEditText.Text = "";
			percentageEditText.Clickable = true;
			percentageEditText.Enabled = true;
			amountEditText.Clickable = false;
			amountEditText.Enabled = false;
			amountEditText.Text = "";
			currentAmount = "";
		}

		void Adapter_NewCoupSelected (object sender, HasFransFranAdapter.NewCoupArgs e)
		{
			creatingFullDeal = true;
			ClearDealCreation ();

			FranchiseInfo fran = e.franForCoup;
			franForNewCoupon = e.franForCoup;

			storeTextView.Text = fran.name;

			dealCreationLayout.Visibility = ViewStates.Visible;
			dealCreationLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
		}

		void FranWithCoup_ConectClover (object sender, FransWithCoups.ConectCloverArgs e)
		{
			creatingFullDeal = false;
			ClearDealCreation ();

			couponBlocker.Visibility = ViewStates.Visible;

			Coupon couponAddingOn = e.selectedCoupon;
			couponForNewCloverDiscount = e.selectedCoupon;

			offerEditText.Text = couponAddingOn.primVal;
			itemEditText.Text = couponAddingOn.primLbl;

			prioritizedCompoundButton.Checked = couponAddingOn.isActive;

			expirationText.Text = couponAddingOn.localExpTime;

			selectRewardPointsButton.Text = couponAddingOn.pointsNeeded.ToString();

			if (couponAddingOn.totalUses > 0) totalUsesEditText.Text = couponAddingOn.totalUses.ToString();
			if (couponAddingOn.maxUses > 0) maxUsesEditText.Text = couponAddingOn.maxUses.ToString();

			dealCreationLayout.Visibility = ViewStates.Visible;
			dealCreationLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
		}

		void DealSubmitButton_Click (object sender, EventArgs e)
		{
			if (creatingFullDeal)
			{
				AndHUD.Shared.Show (this, "Creating Offer...", timeout: null);
				// Adding a full coupon to the franchise

				if (!CheckCarrotCoupFields ())
				{
					AndHUD.Shared.Dismiss (this);
					return;
				}
				if (!CheckCloverDiscountFields ())
				{
					AndHUD.Shared.Dismiss (this);
					return;
				}

				var formattedExpirationDate = expirationDate.ToString ("MM/dd/yyyy h:mm tt");
				string postString = "labelColor=rgb(105,11,0)&foreColor=rgb(255,255,255)&backColor=rgb(255,102,0)&primLabel=" + HttpUtility.UrlEncode (itemEditText.Text)
					+ "&primValue=" + HttpUtility.UrlEncode (offerEditText.Text) + "&expires=" + formattedExpirationDate;

				if (!string.IsNullOrWhiteSpace (maxUsesEditText.Text))
					maxUses = long.Parse (maxUsesEditText.Text);
				else maxUses = -1;
				if (!string.IsNullOrWhiteSpace (totalUsesEditText.Text))
					totalUses = long.Parse (totalUsesEditText.Text);
				else totalUses = -1;
				if (!string.IsNullOrWhiteSpace (maxUsesEditText.Text) && maxUses > 0)
				{
					if (!string.IsNullOrWhiteSpace (totalUsesEditText.Text) && totalUses > 0)
					{
						if (totalUses < maxUses)
							postString += "&maxUses=" + totalUses;
						else postString += "&maxUses=" + maxUses;
					}
					else postString += "&maxUses=" + maxUses;
				}
				else postString += "&maxUses=-1";

				if (!string.IsNullOrWhiteSpace (totalUsesEditText.Text) && totalUses > 0)
					postString += "&totalUses=" + totalUses;
				else postString += "&totalUses=-1";

				if (dealRewardPoints > 0)
					postString += "&requiredPoints=" + dealRewardPoints;

				postString += "&franchiseID=" + franForNewCoupon.franID;
				if (franForNewCoupon.name.Length > 20)
					postString += "&logoText=" + HttpUtility.UrlEncode (franForNewCoupon.name.Substring (0, 20));
				else postString += "&logoText=" + HttpUtility.UrlEncode (franForNewCoupon.name);

				ICloverDiscount thisDiscount;
				string coupString = "";
				if (percentageRadioButton.Checked)
				{
					thisDiscount = new PercentageDiscount (cloverDiscountName.Text, long.Parse (percentageEditText.Text), dealItemID);
					coupString = JsonConvert.SerializeObject (thisDiscount);
				}
				else
				{
					string amountString = amountEditText.Text;
					amountString = amountString.Replace (".", "");
					while (amountString.StartsWith ("0"))
						amountString = amountString.Remove (0, 1);

					long amountCents = long.Parse (amountString);
					thisDiscount = new AmountDiscount (cloverDiscountName.Text, amountCents, dealItemID);
					coupString = JsonConvert.SerializeObject (thisDiscount);
				}
				postString += "&cloverData=" + coupString;
				new Thread (new ThreadStart (delegate
				{
					try
					{
						WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/createoffer.aspx", "POST", postString, (thisAnswer) =>
						{
							var statusCode = thisAnswer.statusCode;
							var answer = thisAnswer.answer;


							if (statusCode == HttpStatusCode.Accepted)
							{
								if (prioritized)
								{
									string activatePostString = "offerID=" + answer + "&franID=" + franForNewCoupon.franID;
									WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/activateoffer.aspx", "POST", activatePostString, (thisAnswer2) =>
									{
										var statusCode2 = thisAnswer2.statusCode;
										var answer2 = thisAnswer2.answer;

										AndHUD.Shared.Dismiss (this);
										RunOnUiThread (() =>
										{
											if (statusCode2 != HttpStatusCode.Accepted)
												Alerts.PresentAlert (this, "Error", "There was an error setting this as your prioritized deal. Please try setting it manualy");
											else Toast.MakeText (this,"Success!",ToastLength.Short).Show ();
											DismissDealCreationAndRefresh ();
										});
									});
								}
								else
								{
									AndHUD.Shared.Dismiss (this);
									RunOnUiThread (() =>
									{
										Toast.MakeText (this,"Success!",ToastLength.Short).Show ();
										DismissDealCreationAndRefresh ();
									});
								}
							}
							else
							{
								AndHUD.Shared.Dismiss (this);
								if (answer.StartsWith ("E1:") || answer.StartsWith ("E2:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "There is a problem with the submitted reward point cost. Please try again with a different amount."));
								else if (answer.StartsWith ("E3:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "Max uses cannot be greater than 99,999"));
								else if (answer.StartsWith ("E4:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "Total uses cannot be greater than 99,999"));
								else if (answer.StartsWith ("E5:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "Max uses cannot be greater than total uses"));
								else if (answer.StartsWith ("E8:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "You are not an editor of this store. Promote this device in the CARROT Mobile app"));
								else if (answer.StartsWith ("E11:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "This store does not have an active subscription. Please try again after resubscribing"));
								else if (answer.StartsWith ("E13:"))
									RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "You have already reached the max amount of offers for this store."));
								else RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "There was an error when trying to create this offer, please try again"));
									
								RunOnUiThread (() => DismissDealCreationAndRefresh ());
							}
						});
					}
					catch (Exception ex)
					{
						AndHUD.Shared.Dismiss (this);
						RunOnUiThread (() =>
						{
							Alerts.PresentAlert (this, "Error", "There was an error when trying to create this offer, please try again");
							DismissDealCreationAndRefresh ();
						});
					}
				})).Start ();
			}
			else
			{
				// Adding a clover deal to an existing coupon
				AndHUD.Shared.Show (this, "Linking Clover...",timeout:null);

				if (!CheckCloverDiscountFields ())
				{
					AndHUD.Shared.Dismiss (this);
					return;
				}

				ICloverDiscount thisDiscount;
				string coupString = "";
				if (percentageRadioButton.Checked)
				{
					thisDiscount = new PercentageDiscount (cloverDiscountName.Text, long.Parse (percentageEditText.Text), dealItemID);
					coupString = JsonConvert.SerializeObject (thisDiscount);
				}
				else
				{
					string amountString = amountEditText.Text;
					amountString.Replace (".", "");
					while (amountString.StartsWith ("0"))
						amountString = amountString.Remove (0, 1);

					long amountCents = long.Parse (amountString);
					thisDiscount = new AmountDiscount (cloverDiscountName.Text, amountCents, dealItemID);
					coupString = JsonConvert.SerializeObject (thisDiscount);
				}
				string postString = "offerID=" + couponForNewCloverDiscount.offerID + "&cloverData=" + coupString;

				new Thread (new ThreadStart (delegate
				{
					try
					{
						WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/addcloverdatatooffer.aspx","POST",postString, (thisAnswer) =>
						{
							var statusCode = thisAnswer.statusCode;
							var answer = thisAnswer.answer;

							AndHUD.Shared.Dismiss (this);
							if (statusCode == HttpStatusCode.Accepted)
							{
								RunOnUiThread (() =>
								{
									Toast.MakeText (this, "Success!", ToastLength.Short);
									DismissDealCreationAndRefresh ();
								});
							}
							else
							{
								string error = answer.Substring (0,3);
								RunOnUiThread (() =>
								{
									switch (error)
									{
										case "E1:" :
											Alerts.PresentAlert (this, "Error", "This device is not an editor of this franchise. Promote this device to an editor using the CARROT Mobile App.");
											break;
										case "E2:" :
											Alerts.PresentAlert (this, "Error", "This store does not have an active subscription. Please renew and try again.");
											break;
										case "E3:" :
											Alerts.PresentAlert (this, "Error", "Could not link discount, please try refreshing and try again.");
											break;
										case "E4:" :
											Alerts.PresentAlert (this, "Error", "This offer already has a Clover Discount linked to it. Please try refreshing.");
											break;
										case "E5:" :
										case "E6:" :
											Alerts.PresentAlert (this, "Error", "This offer is expired or voided, please try refreshing."); 
											break;
										default :
											Alerts.PresentAlert (this, "Error", "Something went wrong when trying to link this Clover Discount, please try again.");
											break;
									}
									DismissDealCreationAndRefresh ();
								});
							}
						});
					}
					catch (Exception ex)
					{
						AndHUD.Shared.Dismiss(this);
						RunOnUiThread (() =>
						{
							Alerts.PresentAlert (this, "Error", "Something went wrong while trying to link this discount, please try again");
							DismissDealCreationAndRefresh ();
						});
					}
				})).Start ();
			}
		}

		void DismissDealCreationAndRefresh ()
		{
			dealCreationLayout.Visibility = ViewStates.Gone;
			dealCreationLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutBottom));
			dealCreationLayout.Animation.AnimationEnd += (sender2, e2) => 
			{
				ClearDealCreation();
			};

			currentFrans.Clear();
			adapter.NotifyDataSetChanged();
			new Thread (new ThreadStart (delegate
			{
				CheckForFranchises();
			})).Start ();
		}

		bool CheckCarrotCoupFields ()
		{
			if (string.IsNullOrWhiteSpace (offerEditText.Text))
			{
				Toast.MakeText (this, "Please enter an Offer", ToastLength.Short).Show ();
				return false;
			}

			if (string.IsNullOrWhiteSpace (itemEditText.Text))
			{
				Toast.MakeText (this, "Please enter an Item", ToastLength.Short).Show ();
				return false;
			}

			return true;
		}

		bool CheckCloverDiscountFields ()
		{
			if (string.IsNullOrWhiteSpace (cloverDiscountName.Text))
			{
				Toast.MakeText (this, "Please enter a Clover discount name", ToastLength.Short).Show ();
				return false;
			}

			if (singleItemRadioButton.Checked)
			{
				if (string.IsNullOrWhiteSpace (dealItemID))
				{
					Toast.MakeText (this, "Please select an item for the discount", ToastLength.Short).Show ();
					return false;
				}
			}

			if (percentageRadioButton.Checked)
			{
				if (string.IsNullOrWhiteSpace (percentageEditText.Text))
				{
					Toast.MakeText (this, "Please enter a percentage amount for the discount", ToastLength.Short).Show ();
					return false;
				}
			}

			if (amountRadioButton.Checked)
			{
				if (string.IsNullOrWhiteSpace (amountEditText.Text))
				{
					Toast.MakeText (this, "Please enter a dollar amount for the discount", ToastLength.Short).Show ();
					return false;
				}
			}
			return true;
		}

		void DealCancelButton_Click (object sender, EventArgs e)
		{
			dealCreationLayout.Visibility = ViewStates.Gone;
			dealCreationLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutBottom));
			dealCreationLayout.Animation.AnimationEnd += (sender2, e2) => 
			{
				ClearDealCreation();
			};
		}

		void AmountEditText_TextChanged (object sender, Android.Text.TextChangedEventArgs e)
		{
			if (e.Text.ToString () != currentAmount)
			{
				string text = e.Text.ToString ().Replace (".", "");

				while (text.Length > 3 && text[0] == '0')
				{
					text = text.Remove (0, 1);
				}
				while (text.Length < 3)
				{
					text = "0" + text;
				}

				text = text.Insert (text.Length - 2, ".");

				currentAmount = text;

				amountEditText.Text = text;
				amountEditText.SetSelection (text.Length);
			}
		}

		void AmountRadioButton_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			if (e.IsChecked)
			{
				amountEditText.Clickable = true;
				amountEditText.Enabled = true;
				amountEditText.RequestFocus ();
			}
			else
			{
				amountEditText.Clickable = false;
				amountEditText.Enabled = false;
			}
		}

		void PercentageRadioButton_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			if (e.IsChecked)
			{
				percentageEditText.Clickable = true;
				percentageEditText.Enabled = true;
				percentageEditText.RequestFocus ();
			}
			else
			{
				percentageEditText.Clickable = false;
				percentageEditText.Enabled = false;
			}
		}

		void SingleItemRadioButton_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			if (e.IsChecked)
			{
				selectItemButton.Clickable = true;
				selectItemButton.Enabled = true;
			}
			else
			{
				selectItemButton.Clickable = false;
				selectItemButton.Enabled = false;
			}
		}

		void WholeOrderRadioButton_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			
		}

		void SelectItemButton_Click (object sender, EventArgs e)
		{
			AndHUD.Shared.Show (this, "Fetching inventory...", timeout: null);
			new Thread (new ThreadStart (delegate
			{
				Com.Clover.Sdk.V3.Inventory.InventoryConnector inventoryConnector = new Com.Clover.Sdk.V3.Inventory.InventoryConnector(this,CloverAccount.GetAccount(this),null);
				inventoryConnector.Connect ();

				if (inventoryConnector.Items.Count <= 0)
				{
					RunOnUiThread (() =>
					{
						AlertDialog alert = new AlertDialog.Builder (this).SetTitle ("No Inventory").SetMessage ("Could not get inventory items, or you have no inventory. Please try again.").Create();
						alert.SetCancelable (false);
						alert.SetCanceledOnTouchOutside (false);
						alert.Show();
					});
					inventoryConnector.Disconnect();
					return;
				}

				List<string> itemStrings = new List<string> ();
				List<Com.Clover.Sdk.V3.Inventory.Item> items = new List<Com.Clover.Sdk.V3.Inventory.Item>();
				foreach (Com.Clover.Sdk.V3.Inventory.Item item in inventoryConnector.Items)
				{
					items.Add (item);
					itemStrings.Add (item.Name + " (" + item.Id + ")");
				}
				ArrayAdapter<string> itemAdapter = new ArrayAdapter<string> (this,Resource.Layout.rewards_points_adapter_row,itemStrings);
				RunOnUiThread (() =>
				{
					AlertDialog alert = new AlertDialog.Builder (this).SetTitle ("Select Item").SetAdapter (itemAdapter, (object sender2, DialogClickEventArgs e2) => 
					{
						var selectedItem = items[e2.Which];
						var selectedItemString = itemStrings [e2.Which];
						selectItemButton.Text = selectedItemString;
						dealItemID = selectedItem.Id;
					}).Create();
					alert.SetCancelable (false);
					alert.SetCanceledOnTouchOutside (false);
					alert.Show ();
				});
				AndHUD.Shared.Dismiss (this);
				inventoryConnector.Disconnect();
			})).Start ();
		}

		void SelectRewardPointsButton_Click (object sender, EventArgs e)
		{
			AlertDialog alert = new AlertDialog.Builder (this).SetTitle ("Select reward Points").SetAdapter (rewardPointsAdapter, RewardPointsSelected).Create ();
			alert.SetCanceledOnTouchOutside(false);
			alert.SetCancelable(false);
			alert.Show ();
		}

		void RewardPointsSelected (object sender, Android.Content.DialogClickEventArgs e)
		{
			dealRewardPoints = long.Parse(points [e.Which].Replace (",",""));
			selectRewardPointsButton.Text = points [e.Which];
		}

		void SelectExpirationDateButton_Click (object sender, EventArgs e)
		{
			dateDialog = new DatePickerDialog (this, ExpirationDatePicked, expirationDate.Year, expirationDate.Month, expirationDate.Day);
			dateDialog.SetCancelable (false);
			dateDialog.SetCanceledOnTouchOutside (false);
			dateDialog.DatePicker.CalendarViewShown = false;
			dateDialog.DatePicker.MaxDate = (long) expirationDate.AddYears(1).Subtract (new DateTime (1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds;
			dateDialog.DatePicker.MinDate = (long) expirationDate.Subtract (new DateTime (1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds;
			dateDialog.UpdateDate (expirationDate);
			dateDialog.Show ();
		}

		void ExpirationDatePicked (object sender, DatePickerDialog.DateSetEventArgs e)
		{
			if (dateDialog != null && !dateDialog.DatePicker.SpinnersShown)
			{
				expirationDate = new DateTime (e.Year, e.MonthOfYear + 1, e.DayOfMonth, 0, 0, 0);
				timeDialog = new TimePickerDialogIntervals (this, ExpirationTimePicked, DateTime.Now.Hour, 0, false);
				timeDialog.SetCancelable (false);
				timeDialog.SetCanceledOnTouchOutside (false);
				timeDialog.Show ();
			}
		}

		void ExpirationTimePicked (object sender, TimePickerDialog.TimeSetEventArgs e)
		{
			expirationDate = new DateTime (expirationDate.Year, expirationDate.Month, expirationDate.Day, e.HourOfDay, e.Minute, 0);
			expirationText.Text = expirationDate.ToString ("ddd, MMM d, yyyy '@' hh:mm tt");
		}

		void PrioritizedCompoundButton_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			prioritized = e.IsChecked;
		}
		#endregion

		void InfoCloseButton_Click (object sender, EventArgs e)
		{
			infoLayout.Visibility = ViewStates.Gone;
			infoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
			infoLayout.Animation.AnimationEnd += (sender2, e2) => infoButton.Clickable = true;
		}

		void InfoButton_Click (object sender, EventArgs e)
		{
			infoButton.Clickable = false;
			infoLayout.Visibility = ViewStates.Visible;
			infoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
		}

		void RegisterButton_Click (object sender, EventArgs e)
		{
			state = MainState.Contact;

			ChangeTitle ("SET UP");

			contactWebview.LoadUrl ("http://www.carrotpass.com/");

			introLayout.Visibility = ViewStates.Gone;
			introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutBottom));

			contactInfoLayout.Visibility = ViewStates.Visible;
			contactInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
		}

		void ScanQRButton_Click1 (object sender, EventArgs e)
		{
			state = MainState.Scanning;

			ChangeTitle ("SET UP");

			introLayout.Visibility = ViewStates.Gone;
			introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutBottom));

			scannerText.Text = Resources.GetString(Resource.String.qrscanner_text);
			scanningLayout.Visibility = ViewStates.Visible;
			nextButton.Visibility = ViewStates.Visible;
			addStoreButton.Visibility = ViewStates.Gone;
			scannerFrame.Visibility = ViewStates.Visible;
			scannerFrame.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInLeft));
			scannerInfoLayout.Visibility = ViewStates.Visible;
			scannerInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInRight));

			scannerInfoLayout.Animation.AnimationEnd += (sender2, e2) => 
			{
				StartScanner ();
			};
		}

		void AddStoreButton_Click (object sender, EventArgs e)
		{
			ChangeTitle ("ADD STORE");

			scannerText.Text = Resources.GetString(Resource.String.add_store_scanner_text);

			hasFransLayout.Visibility = ViewStates.Gone;
			hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));

			addStoreButton.Visibility = ViewStates.Gone;
			addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));

			infoButton.Visibility = ViewStates.Gone;
			infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
			if (infoLayout.Visibility == ViewStates.Visible)
			{
				infoLayout.Visibility = ViewStates.Gone;
				infoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
				infoLayout.Animation.AnimationEnd += (sender2, e2) => infoButton.Clickable = true;
			}

			scanningLayout.Visibility = ViewStates.Visible;
			scannerFrame.Visibility = ViewStates.Visible;
			scannerInfoLayout.Visibility = ViewStates.Visible;

			scannerFrame.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInLeft));
			scannerInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInRight));

			nextButton.Visibility = ViewStates.Gone;

			scannerInfoLayout.Animation.AnimationEnd += (sender2, e2) => 
			{
				StartScanner ();
			};
		}

		void ContactCancelButton_Click (object sender, EventArgs e)
		{			
			ClearContactForm ();
			contactWebview.LoadUrl ("about:blank");

			state = MainState.Intro;

			ChangeTitle ("CARROT");

			introLayout.Visibility = ViewStates.Visible;
			introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));

			contactInfoLayout.Visibility = ViewStates.Gone;
			contactInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutBottom));

			nextButton.Visibility = ViewStates.Visible;
		}

		void NextButton_Click (object sender, EventArgs e)
		{
			AndHUD.Shared.Show (this, "Verifying...", timeout: null);
			new Thread (new ThreadStart (delegate
			{
				CheckForFranchises ();
			})).Start ();
		}

		void CancelScanningButton_Click (object sender, EventArgs e)
		{ 			
			RunOnUiThread (() =>
			{
				SupportFragmentManager.BeginTransaction ()
					.Remove (scanFragment)
					.Commit ();
			});

			if (state == MainState.HasFrans)
			{
				ChangeTitle ("DEALS");

				scannerFrame.Visibility = ViewStates.Gone;
				scannerFrame.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));
				scannerFrame.Animation.AnimationEnd += (sender2, e2) => 
				{
					RunOnUiThread (() => 
					{
						scanningLayout.Visibility = ViewStates.Gone;
						hasFransLayout.Visibility = ViewStates.Visible;
						hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));

						addStoreButton.Visibility = ViewStates.Visible;
						addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInLeft));

						infoButton.Visibility = ViewStates.Visible;
						infoButton.Clickable = true;
						infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
					});
				};
				scannerInfoLayout.Visibility = ViewStates.Gone;
				scannerInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutRight));
			}
			else
			{
				state = MainState.Intro;

				ChangeTitle ("CARROT");

				introLayout.Visibility = ViewStates.Visible;
				introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));

				scannerFrame.Visibility = ViewStates.Gone;
				scannerFrame.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));
				scannerFrame.Animation.AnimationEnd += (sender2, e2) => 
				{
					RunOnUiThread (() => scanningLayout.Visibility = ViewStates.Gone);
				};
				scannerInfoLayout.Visibility = ViewStates.Gone;
				scannerInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutRight));
			}
		}

		void SwipeRefreshLayout_Refresh (object sender, EventArgs e)
		{
			currentFrans.Clear();
			adapter.NotifyDataSetChanged();
			AndHUD.Shared.Show (this, maskType: MaskType.Black, timeout: null);
			new Thread (new ThreadStart (delegate
			{
				CheckForFranchises();
			})).Start ();
		}

		void ClearContactForm ()
		{
			storeNameText.Text = "";
			yourNameText.Text = "";
			emailText.Text = "";
			phoneNumberText.Text = "";
		}

		void ChangeTitle (string title)
		{
			RunOnUiThread (() =>
			{
				headerTitleText.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
				headerTitleText.Animation.AnimationEnd += (sender, e) => 
				{
					RunOnUiThread (() =>
					{
						headerTitleText.Text = title;
						headerTitleText.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
					});
				};
			});
		}

		void StartScanner ()
		{
			
			RunOnUiThread (() =>
			{
				scanOverlay.FindViewById<ImageView> (Resource.Id.scannerBox).SetImageResource (Resource.Drawable.orange_border_square);
				scanOverlay.FindViewById<View> (Resource.Id.scannerHorizontalLine).Visibility = ViewStates.Visible;

				scanFragment = new ZXingScannerFragment ();

				scanFragment.UseCustomOverlayView = true;
				scanFragment.CustomOverlayView = scanOverlay;

				SupportFragmentManager.BeginTransaction ()
					.Add (Resource.Id.Main_scan_scanner, scanFragment)
					.Commit ();

				var opts = new MobileBarcodeScanningOptions {
					PossibleFormats = new List<ZXing.BarcodeFormat> {
						ZXing.BarcodeFormat.QR_CODE
					},
					UseFrontCameraIfAvailable = true,
					CameraResolutionSelector = availableResolutions => {
						foreach (var ar in availableResolutions) {
							Console.WriteLine ("Resolution: " + ar.Width + "x" + ar.Height);
						}
						return null;
					}
				};

				scanFragment.StartScanning (opts,(result) =>
				{
					if (result == null || string.IsNullOrWhiteSpace (result.Text))
					{
						RunOnUiThread (() => Toast.MakeText (this, "Had trouble scanning, try again", ToastLength.Short).Show());
						return;
					}
					RunOnUiThread (() =>
					{
						scanOverlay.FindViewById<ImageView> (Resource.Id.scannerBox).SetImageResource (Resource.Drawable.green_border_square);
						scanOverlay.FindViewById<View> (Resource.Id.scannerHorizontalLine).Visibility = ViewStates.Gone;
						scanFragment.StopScanning();
					});
					HandleScanResult (result.Text);
				});
			});
		}

		void CheckForFranchises ()
		{
			RunOnUiThread (() =>
			{
				currentFrans.Clear();
				adapter.NotifyDataSetChanged ();
			});

			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/getfranchiseinfo.aspx", "GET", "", (thisAnswer) =>
			{
				RunOnUiThread (() => swipeRefreshLayout.Refreshing = false);
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == HttpStatusCode.Accepted)
				{
					AndHUD.Shared.Dismiss(this);

					JArray a = JArray.Parse (answer);

					if (a.Count > 0) //It is linked to a franchise
					{
						foreach (JObject o in a.Children<JObject>())
						{
							var franDict = JsonConvert.DeserializeObject<Dictionary<string,object>> (o.ToString());

							FransWithCoups franWithCoup = new FransWithCoups (this, new FranchiseInfo(franDict));
							franWithCoup.ConectClover += FranWithCoup_ConectClover;
							currentFrans.Add (franWithCoup);
						}

						RunOnUiThread (() => adapter.NotifyDataSetChanged());
						ChangeTitle ("DEALS");
						if (state == MainState.Intro)
						{
							RunOnUiThread (() =>
							{
								state = MainState.HasFrans;
								hasFransLayout.Visibility = ViewStates.Visible;
								hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
								addStoreButton.Visibility = ViewStates.Visible;
								addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
								infoButton.Visibility = ViewStates.Visible;
								infoButton.Clickable = true;
								infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
							});
						}

						else if (state == MainState.Scanning)
						{
							RunOnUiThread (() =>
							{
								state = MainState.HasFrans;

								SupportFragmentManager.BeginTransaction ()
									.Remove (scanFragment)
									.Commit ();

								scannerFrame.Visibility = ViewStates.Gone;
								scannerInfoLayout.Visibility = ViewStates.Gone;
								scannerFrame.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));
								scannerInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutRight));
								scannerFrame.Animation.AnimationEnd += (sender, e) => 
								{
									RunOnUiThread (() =>
									{
										scanningLayout.Visibility = ViewStates.Gone;
										hasFransLayout.Visibility = ViewStates.Visible;
										hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
										addStoreButton.Visibility = ViewStates.Visible;
										addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInLeft));
										infoButton.Visibility = ViewStates.Visible;
										infoButton.Clickable = true;
										infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
									});
								};
							});
						}
						else state = MainState.HasFrans;
					}
					else //It has not been linked
					{
						if (state == MainState.Intro)
						{
							ChangeTitle ("CARROT");
							RunOnUiThread (() =>
							{
								addStoreButton.Visibility = ViewStates.Gone;
								introLayout.Visibility = ViewStates.Visible;
								introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
							});
						}
						else if (state == MainState.Scanning)
						{
							RunOnUiThread (() => Toast.MakeText (this, "Still not linked.", ToastLength.Short).Show ());
						}
						else if (state == MainState.HasFrans)
						{
							RunOnUiThread (() =>
							{
								state = MainState.Intro;
								ChangeTitle ("CARROT");
								addStoreButton.Visibility = ViewStates.Gone;
								addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));

								infoButton.Visibility = ViewStates.Gone;
								infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
								if (infoLayout.Visibility == ViewStates.Visible)
								{
									infoLayout.Visibility = ViewStates.Gone;
									infoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
									infoLayout.Animation.AnimationEnd += (sender2, e2) => infoButton.Clickable = true;
								}

								hasFransLayout.Visibility = ViewStates.Gone;
								hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
								hasFransLayout.Animation.AnimationEnd += (sender, e) => 
								{
									RunOnUiThread (() =>
									{
										introLayout.Visibility = ViewStates.Visible;
										introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
									});
								};
							});
						}
					}
				}
				else //Webcall failure, treate it as no franchise
				{					
					if (state == MainState.Intro)
					{
						ChangeTitle ("CARROT");
						RunOnUiThread (() =>
						{
							introLayout.Visibility = ViewStates.Visible;
							introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
						});
					}
					else if (state == MainState.Scanning)
					{
						RunOnUiThread (() => Toast.MakeText (this, "Still not linked.", ToastLength.Short).Show ());
					}
					else if (state == MainState.HasFrans)
					{
						RunOnUiThread (() =>
						{
							state = MainState.Intro;
							ChangeTitle ("CARROT");
							addStoreButton.Visibility = ViewStates.Gone;
							addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));

							infoButton.Visibility = ViewStates.Gone;
							infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
							if (infoLayout.Visibility == ViewStates.Visible)
							{
								infoLayout.Visibility = ViewStates.Gone;
								infoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
								infoLayout.Animation.AnimationEnd += (sender2, e2) => infoButton.Clickable = true;
							}

							hasFransLayout.Visibility = ViewStates.Gone;
							hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeOut));
							hasFransLayout.Animation.AnimationEnd += (sender, e) => 
							{
								RunOnUiThread (() =>
								{
									introLayout.Visibility = ViewStates.Visible;
									introLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInBottom));
								});
							};
						});
					}
				}
			});
		}

		void HandleScanResult (string barcode)
		{
			if (barcode.StartsWith ("CVT|"))
			{
				//Its a verification qr lets get them joined
				new Thread (new ThreadStart (delegate
				{
					SubmitVerificationCode (barcode);
				})).Start ();
			}
			else
			{
				//Its bunk
				RunOnUiThread (() => 
				{
					Toast.MakeText (this, "Invalid barcode", ToastLength.Short).Show();
					SupportFragmentManager.BeginTransaction ()
						.Remove (scanFragment)
						.Commit ();
					StartScanner ();
				});
			}
		}

		void SubmitVerificationCode (string barcode)
		{
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/verifyfranchise.aspx","POST", "verificationToken=" + barcode, (thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == HttpStatusCode.Accepted)
				{
					if (answer == "Franchise successfully verified")
						ConsolePrime.WriteLine ("This is not what we want");
					else
					{
						RunOnUiThread (() =>
						{
							AlertDialog alert = new AlertDialog.Builder (this).SetTitle ("Notice").SetMessage ("Would you like to link this device to the scanned store?")
								.SetPositiveButton ("Yes", (s, e) =>
							{
								SubmitRequest (answer);
							}).SetNegativeButton ("No", (s, e) =>
							{
									SupportFragmentManager.BeginTransaction ()
										.Remove (scanFragment)
										.Commit ();
									StartScanner ();
							}).Create ();
							alert.SetCanceledOnTouchOutside (false);
							alert.SetCancelable (false);
							alert.Show ();
						});
					}
				}
				else
				{
					ConsolePrime.WriteLine ("Error submit verification code: " + answer);
					string error = answer.Substring (0, 2);
					if (error == "E3")
						RunOnUiThread (() =>Alerts.PresentAlert (this, "Error", "You are already an admin for this store."));
					else if (error == "E4")
						RunOnUiThread (() =>Alerts.PresentAlert (this, "Error", "Your token was invalid or your store was not found, please try again."));
					else
						RunOnUiThread (() =>Alerts.PresentAlert (this, "Error", "An unknown error occured, please try again."));

					RunOnUiThread (() =>
					{
						SupportFragmentManager.BeginTransaction ()
							.Remove (scanFragment)
							.Commit ();
						StartScanner ();
					});
				}
			});
		}

		void SubmitRequest (string franID)
		{
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/requestadminprivileges.aspx", "POST", "franID=" + franID + "&forEditor=false", (thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == System.Net.HttpStatusCode.Accepted)
				{
					if (state == MainState.Scanning)
						RunOnUiThread (() => Alerts.PresentAlert (this, "Success", "You have submitted a request to link this device to your store. Once you have accepted the request in the CARROT Mobile app, hit the NEXT button to get started."));
					else 
						RunOnUiThread (() => 
						{
							Toast.MakeText (this, "Request Sent", ToastLength.Short).Show();
							SupportFragmentManager.BeginTransaction ()
								.Remove (scanFragment)
								.Commit ();
							state = MainState.HasFrans;
							scannerFrame.Visibility = ViewStates.Gone;
							scannerInfoLayout.Visibility = ViewStates.Gone;
							scannerFrame.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutLeft));
							scannerInfoLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideOutRight));
							scannerFrame.Animation.AnimationEnd += (sender, e) => 
							{
								RunOnUiThread (() =>
								{
									scanningLayout.Visibility = ViewStates.Gone;
									hasFransLayout.Visibility = ViewStates.Visible;
									hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
									addStoreButton.Visibility = ViewStates.Visible;
									addStoreButton.StartAnimation (AnimationUtils.LoadAnimation (this, Resource.Xml.SlideInLeft));
									infoButton.Visibility = ViewStates.Visible;
									infoButton.Clickable = true;
									infoButton.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
								});
							};
						});
				}
				else
				{
					if (answer.StartsWith ("E1"))
						RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "You have already requested to become an admin."));
					else if (answer.StartsWith ("E2"))
						RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "You have already requested to become an editor."));
					else if (answer.StartsWith ("E3"))
						RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "You are already an editor."));
					else if (answer.StartsWith ("E4"))
						RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "You are already an admin."));
					else
						RunOnUiThread (() => Alerts.PresentAlert (this, "Error", "Request failed. Please try again."));

					RunOnUiThread (() =>
					{
						SupportFragmentManager.BeginTransaction ()
							.Remove (scanFragment)
							.Commit ();
						StartScanner ();
					});
				}
			});
		}
//
//		void CheckForFranchises ()
//		{
//			if (!refreshingHasFran)
//			{
//				RunOnUiThread (() =>
//				{
//					addStoreButton.Visibility = ViewStates.Gone;
//				});
//			}
//			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/getfranchiseinfo.aspx", "GET", "", (thisAnswer) =>
//			{
//				AndHUD.Shared.Dismiss(this);
//				var statusCode = thisAnswer.statusCode;
//				var answer = thisAnswer.answer;
//
//				if (statusCode == HttpStatusCode.Accepted)
//				{
//					JArray a = JArray.Parse(answer);
//					if (a.Count > 0)
//					{
//						foreach (JObject o in a.Children<JObject>())
//						{
//							var franDict = JsonConvert.DeserializeObject<Dictionary<string,object>> (o.ToString());
//
//							FransWithCoups franWithCoup = new FransWithCoups(this, new FranchiseInfo (franDict));
//							currentFrans.Add (franWithCoup);
//						}
//
//						// This device has stores registered to it
//						if (!refreshingHasFran)
//						{
//							RunOnUiThread (() =>
//							{
//								adapter.NotifyDataSetChanged();
//
//								noFransLayout.Visibility = ViewStates.Gone;
//								hasFransLayout.Visibility = ViewStates.Visible;
//								addStoreButton.Visibility = ViewStates.Visible;
//
//								hasFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
//							});
//						}
//						else RunOnUiThread (() => adapter.NotifyDataSetChanged());
//					}
//					else
//					{
//						// This device has no stores registered to it
//						if (!refreshingNoFran)
//						{
//							RunOnUiThread (() =>
//							{
//								noFransLayout.Visibility = ViewStates.Visible;
//								hasFransLayout.Visibility = ViewStates.Gone;
//								addStoreButton.Visibility = ViewStates.Gone;
//
//								noFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
//							});
//						}
//						else
//						{
//							RunOnUiThread (() => Toast.MakeText(this,"Still not linked", ToastLength.Short).Show());
//						}
//					}
//				}
//				else
//				{
//					if (!refreshingNoFran)
//					{
//						RunOnUiThread (() =>
//						{
//							noFransLayout.Visibility = ViewStates.Visible;
//							hasFransLayout.Visibility = ViewStates.Gone;
//							addStoreButton.Visibility = ViewStates.Gone;
//
//							noFransLayout.StartAnimation (AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn));
//						});
//					}
//				}
//				refreshingHasFran = false;
//				refreshingNoFran = false;
//				RunOnUiThread (() => swipeRefreshLayout.Refreshing = false);
//			});
//		}

	}

	public class TimePickerDialogIntervals : TimePickerDialog
	{
		public const int TimePickerInterval = 30;
		private bool _ignoreEvent = false;
		public TimePicker myPicker;

		public TimePickerDialogIntervals(Context context, EventHandler<TimePickerDialog.TimeSetEventArgs> callBack, int hourOfDay, int minute, bool is24HourView)
			: base(context, (sender, e) => {
				callBack (sender, new TimePickerDialog.TimeSetEventArgs (e.HourOfDay, e.Minute * TimePickerInterval));
			}, hourOfDay, minute/TimePickerInterval, is24HourView)
		{

		}

		public TimePickerDialogIntervals(Context context,int theme, EventHandler<TimePickerDialog.TimeSetEventArgs> callBack, int hourOfDay, int minute, bool is24HourView)
			: base(context,theme, (sender, e) => {
				callBack (sender, new TimePickerDialog.TimeSetEventArgs (e.HourOfDay, e.Minute * TimePickerInterval));
			}, hourOfDay, minute/TimePickerInterval, is24HourView)
		{
		}

		protected TimePickerDialogIntervals(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{

		}

		public override void SetView(View view)
		{
			SetupMinutePicker (view);
			base.SetView(view);
		}

		void SetupMinutePicker (View view)
		{
			var numberPicker = FindMinuteNumberPicker (view as ViewGroup);
			if (numberPicker != null) {
				numberPicker.MinValue = 0;
				numberPicker.MaxValue = 1;
				numberPicker.SetDisplayedValues (new String[] { "00", "30"});
			}
		}

		protected override void OnCreate (Android.OS.Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			GetButton((int)DialogButtonType.Negative).Visibility = Android.Views.ViewStates.Gone;
			this.SetCanceledOnTouchOutside (false);
			this.SetCancelable (false);

		}

		private NumberPicker FindMinuteNumberPicker(ViewGroup viewGroup)
		{
			for (var i = 0; i < viewGroup.ChildCount; i++)
			{
				var child = viewGroup.GetChildAt(i);
				var numberPicker = child as NumberPicker;
				if (numberPicker != null)
				{
					if (numberPicker.MaxValue == 59)
					{
						return numberPicker;
					}
				}

				var childViewGroup = child as ViewGroup;
				if (childViewGroup != null)
				{
					var childResult = FindMinuteNumberPicker (childViewGroup);
					if(childResult !=null)
						return childResult;
				}
			}

			return null;
		}
	}
}
