﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;

using Android.App;
using Android.Support.V4.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;

namespace CarrotClover
{
	public static class JLHelper
	{
		public static int MeasureHeight (Context context, RelativeLayout layout)
		{
			Android.Util.DisplayMetrics metrics = new Android.Util.DisplayMetrics ();
			((Activity)context).WindowManager.DefaultDisplay.GetMetrics (metrics);

			int width = metrics.WidthPixels;
			var widthSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec (width, MeasureSpecMode.Exactly);
			var heightSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified);

			layout.Measure (widthSpec, heightSpec);
			return layout.MeasuredHeight;
		}

		public static int MeasureHeight (Context context, LinearLayout layout)
		{
			Android.Util.DisplayMetrics metrics = new Android.Util.DisplayMetrics ();
			((Activity)context).WindowManager.DefaultDisplay.GetMetrics (metrics);

			int width = metrics.WidthPixels;
			var widthSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec (width, MeasureSpecMode.Exactly);
			var heightSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified);

			layout.Measure (widthSpec, heightSpec);
			return layout.MeasuredHeight;
		}

		public static void AnimateLayout (RelativeLayout layout, int startHeight, int endHeight, int duration)
		{
			Android.Animation.ValueAnimator expandAnimation = Android.Animation.ValueAnimator.OfInt (startHeight, endHeight);
			expandAnimation.SetDuration (duration);
			expandAnimation.Update += (sender, e) => 
			{
				var animatedHeight = (int)expandAnimation.AnimatedValue;
				var layoutParms = layout.LayoutParameters;
				layoutParms.Height = animatedHeight;
				layout.LayoutParameters = layoutParms;
				layout.RequestLayout();
			};
			expandAnimation.Start ();
		}

		public static void AnimateLayout (LinearLayout layout, int startHeight, int endHeight, int duration)
		{
			Android.Animation.ValueAnimator expandAnimation = Android.Animation.ValueAnimator.OfInt (startHeight, endHeight);
			expandAnimation.SetDuration (duration);
			expandAnimation.Update += (sender, e) => 
			{
				var animatedHeight = (int)expandAnimation.AnimatedValue;
				var layoutParms = layout.LayoutParameters;
				layoutParms.Height = animatedHeight;
				layout.LayoutParameters = layoutParms;
				layout.RequestLayout();
			};
			expandAnimation.Start ();
		}

		public static int ConvertPixelsToDp(Context context,float pixelValue)
		{
			var resources = context.Resources;
			var metrics = resources.DisplayMetrics;
			float dp = pixelValue / ((float)metrics.DensityDpi / 160f);
			return (int)Math.Ceiling(dp);
		}

		public static int ConvertDpToPixels(Context context, float dpValue)
		{
			var resources = context.Resources;
			var metrics = resources.DisplayMetrics;
			float px = dpValue * ((float)metrics.DensityDpi / 160f);
			return (int)Math.Ceiling(px);
		}
	}
}

