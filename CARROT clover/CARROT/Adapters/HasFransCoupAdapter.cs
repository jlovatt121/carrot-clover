﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;

using Android.App;
using Android.Support.V4.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;

using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

using Com.Clover.Sdk.Util;
using Com.Clover.Sdk.V3.Employees;

using AndroidHUD;

namespace CarrotClover
{
	public class HasFransFranAdapter : ArrayAdapter<FransWithCoups>
	{
		public class ViewHolder : Java.Lang.Object
		{
			public TextView franText;

			public TextView errorText;

			public RelativeLayout coupLayout;
			public ProgressBar coupSpinner;

			public Button chevron;
			public ImageView chevronImage;

			public BetterButton newCoupButton;

			public LinearLayout coupList;
		}

		public class HolderArgs : Java.Lang.Object
		{
			public ViewHolder holder;
			public FransWithCoups item;

			public HolderArgs ()
			{}

			public HolderArgs (ViewHolder Holder, FransWithCoups Item)
			{
				this.holder = Holder;
				this.item = Item;
			}
		}

		public class NewCoupArgs : EventArgs
		{
			public FranchiseInfo franForCoup;

			public NewCoupArgs (FranchiseInfo FranForCoup)
			{
				this.franForCoup = FranForCoup;
			}
		}

		public List<FransWithCoups> items = new List<FransWithCoups> ();
		int resourceID;
		Context context;

		public event EventHandler<NewCoupArgs> NewCoupSelected;

		public HasFransFranAdapter (Context Context, int ResourceID, List<FransWithCoups> Items) : base (Context, ResourceID, Items)
		{
			this.context = Context;
			this.resourceID = ResourceID;
			this.items = Items;
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder;
			FransWithCoups item = items [position];

			if (convertView == null)
			{
				convertView = ((Activity)context).LayoutInflater.Inflate (resourceID, parent, false);
				holder = new ViewHolder ();

				holder.franText = convertView.FindViewById<TextView> (Resource.Id.storeName);
				holder.errorText = convertView.FindViewById<TextView> (Resource.Id.errorText);

				holder.coupLayout = convertView.FindViewById<RelativeLayout> (Resource.Id.couponLayout);
				holder.coupList = convertView.FindViewById<LinearLayout> (Resource.Id.coupList);

				holder.chevron = convertView.FindViewById<Button> (Resource.Id.chevron);
				holder.chevron.Click += Holder_chevron_Click;

				holder.chevronImage = convertView.FindViewById<ImageView> (Resource.Id.chevronImage);

				holder.newCoupButton = convertView.FindViewById<BetterButton> (Resource.Id.newCoupButton);
				holder.newCoupButton.Click += Holder_newCoupButton_Click;

				holder.coupSpinner = convertView.FindViewById<ProgressBar> (Resource.Id.loadingSpinner);

				convertView.SetTag (Resource.Layout.has_fran_list_item, holder);
			}
			else holder = (ViewHolder)convertView.GetTag (Resource.Layout.has_fran_list_item);

			HolderArgs thisArg = new HolderArgs (holder, item);
			holder.chevron.SetTag (Resource.Id.chevron, thisArg);
			holder.newCoupButton.SetTag (Resource.Id.newCoupButton, thisArg);

			item.holder = holder;

			holder.franText.Text = item.fran.name;

			if (!item.startedGettingCoups) item.GetCoups ();

			return convertView;
		}

		void Holder_chevron_Click (object sender, EventArgs e)
		{
			HolderArgs thisArg = (HolderArgs)((Button)sender).GetTag (Resource.Id.chevron);
			ViewHolder holder = thisArg.holder;

			if (thisArg.item.expanded)
			{
				//Its expanded, lets shrink it
				holder.chevronImage.StartAnimation (AnimationUtils.LoadAnimation(context, Resource.Xml.Rotate180));
				holder.chevronImage.Animation.FillEnabled = true;
				holder.chevronImage.Animation.FillAfter = true;
//				holder.chevron.SetImageResource (Resource.Drawable.ic_expand_more_black_48dp);
				JLHelper.AnimateLayout(holder.coupLayout, thisArg.item.expandedHeight, 0, 250);
				thisArg.item.expanded = false;
			}
			else
			{
				//Its shrank, lets expand it
				holder.chevronImage.StartAnimation (AnimationUtils.LoadAnimation(context, Resource.Xml.RotateBack180));
				holder.chevronImage.Animation.FillEnabled = true;
				holder.chevronImage.Animation.FillAfter = true;
//				holder.chevron.SetImageResource (Resource.Drawable.ic_expand_less_black_48dp);
				JLHelper.AnimateLayout(holder.coupLayout, 0, thisArg.item.expandedHeight, 250);
				thisArg.item.expanded = true;
			}
		}

		void Holder_newCoupButton_Click (object sender, EventArgs e)
		{
			HolderArgs thisArg = (HolderArgs)((BetterButton)sender).GetTag (Resource.Id.newCoupButton);
			ViewHolder holder = thisArg.holder;
			FransWithCoups item = thisArg.item;

			AndHUD.Shared.Show (context, "Validating...", timeout: null);
			new Thread (new ThreadStart (delegate
			{
				EmployeeConnector employeeConnector = new EmployeeConnector(context, CloverAccount.GetAccount(context), null);
				employeeConnector.Connect();

				var currentEmployee = employeeConnector.Employee;
				if (currentEmployee.Role.ToString() == "EMPLOYEE")
				{
					AndHUD.Shared.Dismiss(context);
					Alerts.PresentAlert(context,"Permission Error","Only Managers or Admins can add new CARROT Deals");
					employeeConnector.Disconnect ();
					return;
				}

				//TODO GO to offer maker to make a full deal
				AndHUD.Shared.Dismiss(context);
				((Activity)context).RunOnUiThread (() =>
				{
					if (this.NewCoupSelected != null)
						this.NewCoupSelected (this, new NewCoupArgs (item.fran));
				});

//				((Activity)context).RunOnUiThread (() => Toast.MakeText (context, "New coupon clicked for " + item.fran.name, ToastLength.Short).Show ());
				employeeConnector.Disconnect();
			})).Start ();
		}
	}

	public class FransWithCoups
	{
		public class CoupArgs : Java.Lang.Object
		{
			public Coupon thisCoupon;

			public CoupArgs (Coupon ThisCoupon)
			{
				this.thisCoupon = ThisCoupon;
			}
		}

		public class ConectCloverArgs : EventArgs
		{
			public FransWithCoups thisFranWithCoup;
			public Coupon selectedCoupon;

			public ConectCloverArgs (FransWithCoups ThisFranWithCoup, Coupon SelectedCoupon)
			{
				this.selectedCoupon = SelectedCoupon;
				this.thisFranWithCoup = ThisFranWithCoup;
			}
		}
		public event EventHandler<ConectCloverArgs> ConectClover;

		Context context;

		public FranchiseInfo fran;
		public List<Coupon> activeCoups = new List<Coupon>();

		public bool expanded = true;
		public bool startedGettingCoups = false;
		public bool hadTroubleGettingCoups = false;
		public bool finishedGettingCoups = false;

		public int expandedHeight = 0;

		public Coupon prioritizedCoupon;

		public HasFransFranAdapter.ViewHolder holder;

		public FransWithCoups (Context Context)
		{
			this.context = Context;
		}

		public FransWithCoups (Context Context, FranchiseInfo Fran)
		{
			this.context = Context;
			this.fran = Fran;
		}

		public void GetCoups ()
		{
			startedGettingCoups = true;
			activeCoups.Clear ();
			holder.coupList.RemoveAllViews ();

			holder.errorText.Visibility = ViewStates.Gone;
			holder.coupSpinner.Visibility = ViewStates.Visible;
			holder.coupList.Visibility = ViewStates.Visible;
			new Thread (new ThreadStart (delegate
			{
				try
				{
					WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/getunexpiredoffers.aspx", "POST", "franID=" + fran.franID + "&isCloverDevice=true", (thisAnswer) =>
					{
						var answer = thisAnswer.answer;
						var statusCode = thisAnswer.statusCode;

						if (statusCode == HttpStatusCode.Accepted)
						{
							JArray a = JArray.Parse (answer);

							((Activity)context).RunOnUiThread (() =>
							{
								if (a.Count == 3) holder.newCoupButton.Visibility = ViewStates.Gone;
								else holder.newCoupButton.Visibility = ViewStates.Visible;
							});

							if (a.Count > 0)
							{
								bool foundPrioritized = false;
								foreach (JObject o in a.Children<JObject> ())
								{
									var coupDict = JsonConvert.DeserializeObject<Dictionary<string,object>> (o.ToString());

									Coupon coup = new Coupon (coupDict);
									coup.isExpired = false;
									coup.Ovoided = false;
									activeCoups.Add (coup);

									if (!foundPrioritized)
									{
										if (coup.isActive)
										{
											if (prioritizedCoupon != null)
												prioritizedCoupon.isActive = false;
											
											prioritizedCoupon = coup;
											foundPrioritized = true;
										}
										else
										{
											if (prioritizedCoupon == null)
											{
												prioritizedCoupon = coup;
												coup.isActive = true;
											}
											else if (DateTime.ParseExact (coup.localExpTime,"M/d/yy h:mm tt", System.Globalization.CultureInfo.GetCultureInfo ("en-US"))
											         .CompareTo(DateTime.ParseExact (prioritizedCoupon.localExpTime,"M/d/yy h:mm tt", System.Globalization.CultureInfo.GetCultureInfo("en-US"))) < 1)
											{
												prioritizedCoupon.isActive = false;
												prioritizedCoupon = coup;
												coup.isActive = true;
											}
										}
									}
								}
								((Activity)context).RunOnUiThread (() =>
								{
									holder.coupSpinner.Visibility = ViewStates.Gone;
									holder.coupList.Visibility = ViewStates.Visible;
									holder.errorText.Visibility = ViewStates.Gone;	
									SetAllCoupsForFran();
								});
							}
							else
							{
								((Activity)context).RunOnUiThread (() =>
								{
									holder.coupSpinner.Visibility = ViewStates.Gone;
									holder.coupList.Visibility = ViewStates.Gone;
									holder.errorText.Visibility = ViewStates.Visible;
									holder.errorText.Text = "No Active Deals";
									var endHeight = JLHelper.MeasureHeight (context, holder.coupLayout);
									this.expandedHeight = endHeight;
								});
							}
							hadTroubleGettingCoups = false;
							finishedGettingCoups = true;		
						}
						else
						{
							((Activity)context).RunOnUiThread (() =>
							{
								holder.coupList.Visibility = ViewStates.Gone;
								holder.coupSpinner.Visibility = ViewStates.Gone;
								holder.errorText.Visibility = ViewStates.Visible;
								holder.errorText.Text = "Not status code 202";
								var endHeight = JLHelper.MeasureHeight (context, holder.coupLayout);
								this.expandedHeight = endHeight;
							});

							hadTroubleGettingCoups = true;
							finishedGettingCoups = true;
						}
					});
				}
				catch (Exception ex)
				{
					((Activity)context).RunOnUiThread (() =>
					{
						holder.coupList.Visibility = ViewStates.Gone;
						holder.coupSpinner.Visibility = ViewStates.Gone;
						holder.errorText.Visibility = ViewStates.Visible;
						holder.errorText.Text = "Catch Error";
						var endHeight = JLHelper.MeasureHeight (context, holder.coupLayout);
						this.expandedHeight = endHeight;
					});
					finishedGettingCoups = true;
					hadTroubleGettingCoups = true;
				}
			})).Start ();
		}

		public void SetAllCoupsForFran ()
		{
			for (int i = 0; i < activeCoups.Count; i++)
			{
				var view = ((Activity)context).LayoutInflater.Inflate (Resource.Layout.has_fran_coup_item, holder.coupList, false);

				Coupon coup = activeCoups[i];

				CoupArgs thisArg = new CoupArgs (coup);

				view.FindViewById<TextView> (Resource.Id.offerItem).Text = coup.primVal + " " + coup.primLbl;

				view.FindViewById<ImageButton> (Resource.Id.prioritizeButton).SetTag (Resource.Id.prioritizeButton, thisArg);
				view.FindViewById<ImageButton> (Resource.Id.prioritizeButton).Click += Prioritize_button_click;

				view.FindViewById<ImageButton> (Resource.Id.cloverButton).SetTag (Resource.Id.cloverButton, thisArg);
				view.FindViewById<ImageButton> (Resource.Id.cloverButton).Click += Clover_button_click;

				if (coup.isActive)
				{											
					view.FindViewById<ImageButton> (Resource.Id.prioritizeButton).SetImageResource (Resource.Drawable.star_1_hdpi);// = "Prioritized";
					view.FindViewById<ImageButton> (Resource.Id.prioritizeButton).Clickable = false;
				}
				else
				{
					view.FindViewById<ImageButton> (Resource.Id.prioritizeButton).SetImageResource (Resource.Drawable.star_0_hdpi);// = "Prioritize";
					view.FindViewById<ImageButton> (Resource.Id.prioritizeButton).Clickable = true;
				}

				if (coup.cloverDiscount != null)
				{
					view.FindViewById<ImageButton> (Resource.Id.cloverButton).SetImageResource (Resource.Drawable.clover_orange_hdpi);// = "Has Clover Discount";
					view.FindViewById<ImageButton> (Resource.Id.cloverButton).Clickable = false;
				}
				else
				{
					view.FindViewById<ImageButton> (Resource.Id.cloverButton).SetImageResource (Resource.Drawable.clover_grey_hdpi);// = "Add Clover Discount";
					view.FindViewById<ImageButton> (Resource.Id.cloverButton).Clickable = true;
				}

				if (i == activeCoups.Count - 1)
					view.FindViewById<View> (Resource.Id.divider).Visibility = ViewStates.Invisible;
				else view.FindViewById<View> (Resource.Id.divider).Visibility = ViewStates.Visible;

				holder.coupList.AddView (view);
			}

			var endHeight = JLHelper.MeasureHeight (context, holder.coupLayout);
			this.expandedHeight = endHeight;
		}

		void Clover_button_click (object sender, EventArgs e)
		{
			CoupArgs thisArg = (CoupArgs)((ImageButton)sender).GetTag (Resource.Id.cloverButton);
			Coupon coup = thisArg.thisCoupon;

			if (this.fran.isEditor || this.fran.isOwner)
			{
				AndHUD.Shared.Show (context, "Validating...", timeout: null);
				new Thread (new ThreadStart (delegate
				{
					EmployeeConnector employeeConnector = new EmployeeConnector (context, CloverAccount.GetAccount (context), null);
					employeeConnector.Connect ();

					var currentEmployee = employeeConnector.Employee;
					if (currentEmployee.Role.ToString () == "EMPLOYEE")
					{
						AndHUD.Shared.Dismiss (context);
						Alerts.PresentAlert (context, "Permission Error", "Only Managers or Owners can link Clover Discounts to CARROT Deals");
						employeeConnector.Disconnect ();
						return;
					}

					//TODO GO to offer maker to link a discount
					AndHUD.Shared.Dismiss (context);
					((Activity)context).RunOnUiThread (() =>
					{
						if (this.ConectClover != null)
							this.ConectClover (this,new ConectCloverArgs(this,coup));
					});					
					employeeConnector.Disconnect ();
				})).Start ();
			}
			else
			{
				Alerts.PresentAlert (context, "Permission Error", "You need to make this device an editor for your store. You can do this inside the CARROT mobile app.");
			}
		}

		void Prioritize_button_click (object sender, EventArgs e)
		{
			if (this.fran.isEditor || this.fran.isOwner)
			{
				CoupArgs thisArg = (CoupArgs)((ImageButton)sender).GetTag (Resource.Id.prioritizeButton);
				Coupon coup = thisArg.thisCoupon;

				new Thread (new ThreadStart (delegate
				{
					EmployeeConnector employeeConnector = new EmployeeConnector (context, CloverAccount.GetAccount (context), null);
					employeeConnector.Connect ();

					var currentEmployee = employeeConnector.Employee;
					if (currentEmployee.Role.ToString () == "EMPLOYEE")
					{
						AndHUD.Shared.Dismiss (context);
						Alerts.PresentAlert (context, "Permission Error", "Only Managers or Owners can link Clover Discounts to CARROT Deals");
						employeeConnector.Disconnect ();
						return;
					}
					AndHUD.Shared.Dismiss (context);
					employeeConnector.Disconnect ();
					PrioritizeCoupOnServer (coup);
				})).Start ();
			}
			else
			{
				Alerts.PresentAlert (context, "Permission Error", "You need to make this device an editor for your store. You can do this inside the CARROT mobile app.");
			}
		}

		public void PrioritizeCoupOnServer (Coupon coup)
		{
			AndHUD.Shared.Show (context, "Prioritizing...", maskType: MaskType.Black, timeout: null);
			WebCalls.MakeAuthWebCall ("https://carrot.azurewebsites.net/activateoffer.aspx", "POST", "franID=" + System.Web.HttpUtility.UrlEncode (this.fran.franID.ToString()) + "&offerID=" + coup.offerID, (thisAnswer) =>
			{
				var statusCode = thisAnswer.statusCode;
				var answer = thisAnswer.answer;

				if (statusCode == HttpStatusCode.Accepted)
				{
					if (this.prioritizedCoupon == null)
					{
						// Somehow the prioritized coupon was not set, lets fix that
						this.prioritizedCoupon = coup;
						coup.isActive = true;
						((Activity)context).RunOnUiThread (() =>
						{
							this.holder.coupList.RemoveAllViews();
							SetAllCoupsForFran ();
						});
					}
					else
					{
						this.prioritizedCoupon.isActive = false;
						coup.isActive = true;
						this.prioritizedCoupon = coup;
						((Activity)context).RunOnUiThread (() =>
						{
							this.holder.coupList.RemoveAllViews();
							SetAllCoupsForFran ();
						});
					}
				}
				else
				{
					((Activity)context).RunOnUiThread (() =>
					{
						Toast.MakeText (context, "Prioritizing failed. Try Again.", ToastLength.Short).Show();
					});
					int i = 0;
				}
				AndHUD.Shared.Dismiss(context);
			});
		}
	}
}

