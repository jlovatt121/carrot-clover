﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Com.Clover.Sdk.V3.Order;
using Com.Clover.Sdk.V3.Inventory;

namespace CarrotClover
{
	public class LineItemPickerAdapter : ArrayAdapter<LineItem>
	{
		List<LineItem> items = new List<LineItem>();
		int layoutID;
		Context context;

		public LineItemPickerAdapter (Context Context, int LayoutID, List<LineItem> Items) : base (Context,LayoutID,Items)
		{
			this.context = Context;
			this.layoutID = LayoutID;
			this.items = Items;
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder viewHolder;
			LineItem lineItem = items [position];

			if (convertView == null)
			{
				convertView = ((Activity)context).LayoutInflater.Inflate (layoutID, parent, false);
				viewHolder = new ViewHolder ();

				viewHolder.itemName = convertView.FindViewById<TextView> (Resource.Id.lineItemName);

				viewHolder.currentDiscounts = convertView.FindViewById<TextView> (Resource.Id.lineItemDiscounts);

				convertView.SetTag (Resource.Layout.line_item_picker_row, viewHolder);
			} 
			else viewHolder = (ViewHolder)convertView.GetTag (Resource.Layout.line_item_picker_row);

			viewHolder.itemName.Text = lineItem.Name;

			string discountText = "- -";
			if (lineItem.HasDiscounts)
			{
				discountText = "";
				foreach (Com.Clover.Sdk.V3.Order.Discount discount in lineItem.Discounts)
				{
					discountText += discount.Name + ", ";
				}
				discountText = discountText.Substring (0, discountText.Length - 2);
			}

			viewHolder.currentDiscounts.Text = discountText;

			return convertView;
		}

		public class ViewHolder : Java.Lang.Object
		{
			public TextView itemName;
			public TextView currentDiscounts;
		}
	}
}

